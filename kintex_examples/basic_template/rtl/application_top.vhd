library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.components.all;

entity application_top is
	port(
				clk   :in std_logic;
				reset :in std_logic;
				-- these will all become vectors when we move to 4 motors
				-- Stepper Motor Signals
				m_hflf :in std_logic_vector(3 downto 0); --high frequency limit fault
				m_lflf :in std_logic_vector(3 downto 0); --low frequency limit fault
				m_sdo  :in std_logic_vector(3 downto 0);  --serial data out from the driver
				m_step :out std_logic_vector(3 downto 0); --step input to the driver
				m_dir  :out std_logic_vector(3 downto 0);  --direction input to the driver
				m_en   :out std_logic_vector(3 downto 0);   --enable signal for the mosfets on the stepper driver
				m_sdi  :out std_logic_vector(3 downto 0);  --serial data input to the driver
				m_sclk :out std_logic_vector(3 downto 0); --serial input clock to the driver
				m_csn  :out std_logic_vector(3 downto 0);  --chip select input to the driver
				m_scl  :out std_logic;  --serial clock input to the temperature sensor on the stepper motor board
				m_sda  :inout std_logic;
				-- User Control Signals
				user_switch       :in std_logic;
				user_left_button  :in std_logic;
				user_right_button :in std_logic;
				user_rotary       :in std_logic_vector(7 downto 0);
				-- LED Panel Signals
				led_scl :out std_logic;
				led_sda :inout std_logic

			);
end entity application_top;

architecture rtl of application_top is

	-- Top FSM States
	type state_type is (IDLE, READY, MOTOR_INIT, MOTOR_STROBE, MOTOR_DRIVE, MOTOR_CLEAR);
	signal state : state_type;

	-- Base Signals
	signal reset_n   :std_logic := '0';
	signal mod_reset :std_logic := '1';
	-- Other (in)
	signal interlock_permit : std_logic := '1';

	-- Types for array of motors
	type m_32b is array (3 downto 0) of std_logic_vector(31 downto 0);
	type m_27b is array (3 downto 0) of std_logic_vector(26 downto 0);
	type m_20b is array (3 downto 0) of std_logic_vector(19 downto 0);

	-- Motor Control (in)
	signal m_cntl    :std_logic_vector(31 downto 0);
	signal m_expert  :std_logic_vector(31 downto 0);
	signal m_acc     :std_logic_vector(31 downto 0); -- external, motor acceleration
	signal m_vlcty   :std_logic_vector(31 downto 0); -- external, motor velocity
	signal m_steps   :std_logic_vector(31 downto 0); -- external, stepsto be moved
	signal m_sub_stp :std_logic_vector(31 downto 0); -- external
	signal m_drvI    :std_logic_vector(4 downto 0); -- external

	-- Motor Readouts (out)
	signal m_steps_actual :m_32b;
	signal m_period       :m_27b;
	signal m_clkrate      :m_27b;
	signal m_accum        :m_20b;
	signal m_lacc         :m_32b;
	signal m_lvlcty       :m_32b;
	signal m_lsteps       :m_32b;
	signal m_sgn_steps    :m_32b;
	signal m_abs_steps    :m_32b;
	signal m_status       :m_32b;

	-- Stepper stuff (out)
	signal master_clr    :std_logic_vector(3 downto 0);
	signal mconfig_done  :std_logic_vector(3 downto 0);
	signal mstart_config :std_logic_vector(3 downto 0);
	signal mdrvI_config  :std_logic_vector(3 downto 0);
	signal mdrv_inhibit  :std_logic_vector(3 downto 0);

	-- User Stepper Driver Signals
	signal en_stepper :std_logic;
	signal direction  :std_logic := '0';
	signal step_mode  :std_logic_vector(2 downto 0) := (others => '0');

	-- Stepper Control signals
	signal move     :std_logic := '0';
	signal stop     :std_logic := '1';
	signal inhibit  :std_logic := '1';
	signal clr_done :std_logic := '0';

	-- Stepper Expert Signals
	signal en_sub_abs :std_logic := '0';

	-- Stepper Status Signals
	signal motor_done   :std_logic;
	signal motor_moving :std_logic;

	-- LED Panel Signals
	signal top_leds    :std_logic_vector(7 downto 0) := (others => '0');
	signal bottom_leds :std_logic_vector(7 downto 0) := (others => '0');

	signal delay_counter :std_logic := '0';
	signal direction_buf :std_logic := '0';

begin

	-- Base Signals
	reset_n <= not reset;

	-- Control Signals
	m_cntl(0) <= move;
	m_cntl(2) <= inhibit;
	m_cntl(3) <= stop;
	m_cntl(4) <= clr_done;
	m_cntl(5) <= direction_buf;

	-- Expert Signals
	m_expert(13) <= en_sub_abs;

	-- Status Signals
	motor_moving <= m_status(0)(0) and m_status(1)(0) and m_status(2)(0) and m_status(3)(0);
	motor_done   <= m_status(0)(2) and m_status(1)(2) and m_status(2)(2) and m_status(3)(2);

	-- Direction Latch
	i_direction_buf: latch_n
	port map(
						clock => clk,
						reset => '1',
						clear => '1',
						en => en_stepper,
						inp => direction,
						oup => direction_buf
					);

	-- Instantiate LED Panel
	i_IOExpanderTCA6416ALEDs: entity work.IO_Expander_TCA6416A(rtl)
	port map(
						reset_n => reset_n,
						clock => clk,
						a1_en => '0',
						a0_port0_dir => x"00",
						a0_port1_dir => x"00",
						a1_port0_dir => x"00",
						a1_port1_dir => x"00",
						a0_port0_in => bottom_leds,
						a0_port1_in => top_leds,
						a1_port0_in => x"00",
						a1_port1_in => x"00",
						a0_port0_out => open,
						a0_port1_out => open,
						a1_port0_out => open,
						a1_port1_out => open,
						scl => led_scl,
						sda => led_sda
					);

	-- Control the LEDs
	process(clk)
	begin
		if (rising_edge(clk)) then
			if (mod_reset = '1') then
				-- Main Reset
				bottom_leds(7 downto 0) <= x"00";
				top_leds(7 downto 0) <= x"00";
			else
				bottom_leds(0) <= m_status(0)(0);
				bottom_leds(1) <= m_status(0)(2);
				bottom_leds(2) <= m_status(1)(0);
				bottom_leds(3) <= m_status(1)(2);
				bottom_leds(4) <= m_status(2)(0);
				bottom_leds(5) <= m_status(2)(2);
				bottom_leds(6) <= m_status(3)(0);
				bottom_leds(7) <= m_status(3)(2);
				top_leds(2 downto 0) <= step_mode;
				top_leds(6) <= direction_buf;
				top_leds(7) <= not direction_buf;
			end if;
		end if;
	end process;

	-- Instantiate User Control
	i_UserControlWrapper: entity work.user_control_wrapper(rtl)
	port map(
						-- Inputs
						clock        => clk,
						reset        => mod_reset,
						en_in        => user_switch,
						rotary       => user_rotary,
						left_button  => user_left_button,
						right_button => user_right_button,
						-- Outputs
						en_out       => en_stepper,
						direction    => direction,
						step_mode    => step_mode
					);

	-- Generate Stepper Tops
	g_inst_stepperTop: for i in 0 to 3 generate
		inst_stepperTop : entity work.stepper_top(rtl)
		port map(
							clk => clk,
							-- Other
							interlock_permit => interlock_permit,
							-- Motor signals to off-board devices
							m_hflf => m_hflf(i),
							m_lflf => m_lflf(i),
							m_sdo  => m_sdo(i),
							m_step => m_step(i),
							m_dir  => m_dir(i),
							m_en   => m_en(i),
							m_sdi  => m_sdi(i),
							m_sclk => m_sclk(i),
							m_csn  => m_csn(i),

							m_sda  => m_sda,
							m_scl  => m_scl,

							m_cntl  => m_cntl,
							m_expert => m_expert,
							m_acc => m_acc,
							m_vlcty => m_vlcty,
							m_steps => m_steps,
							m_sub_stp => m_sub_stp,

							-- Stepper Readouts
							m_steps_actual => m_steps_actual(i),
							m_period       => m_period(i),
							m_clkrate      => m_clkrate(i),
							m_accum        => m_accum(i),
							m_lacc         => m_lacc(i),
							m_lvlcty       => m_lvlcty(i), --last motor velocity
							m_lsteps       => m_lsteps(i), --last steps to be moved
							m_sgn_steps    => m_sgn_steps(i), --Signed steps
							m_abs_steps    => m_abs_steps(i), --Absolute steps
							m_status       => m_status(i),

							m_drvI => m_drvI,

							-- Stepper stuff
							master_clr    => '0',
							mconfig_done  => mconfig_done(i),
							mstart_config => mstart_config(i),
							mdrvI_config  => mdrvI_config(i),
							mdrv_inhibit  => mdrv_inhibit(i)

						);

	end generate g_inst_stepperTop;

	-- Top FSM
	process(clk)
	begin
		if (rising_edge(clk)) then
			if (mod_reset = '1') then
				-- Main Reset
				inhibit  <= '1'; -- set inhibit bit to 1
				clr_done <= '0'; -- set clr_done bit to 0
				stop     <= '1'; -- set stop bit to 1
				move <= '0'; -- set move bit to 0
				state <= IDLE;

			else
				case state is

					when IDLE =>
						if (en_stepper = '1') then
							state <= READY;
						end if;

					when READY =>
						-- Move to initialize the motor if the stepper is enabled by the user interface
						-- Otherwise return to idle
						if (en_stepper = '1') then
							case step_mode is
								when "000" =>
									-- Selection 0
									state <= IDLE;

								when "001" =>
									-- Selection 1
									m_steps <= x"00001388";
									m_vlcty <= x"00000D78";
									m_acc   <= x"00000159";
									m_drvI  <= "01011";
									state <= MOTOR_INIT;

								when "010" =>
									-- Selection 2
									m_steps <= x"000F4240";
									m_vlcty <= x"00000D78";
									m_acc   <= x"00000159";
									m_drvI  <= "01011";
									state <= MOTOR_INIT;

								when "011" =>
									-- Selection 3
									m_steps <= x"000F4240";
									m_vlcty <= x"00007530";
									m_acc   <= x"00000FA0";
									m_drvI  <= "01011";
									state <= MOTOR_INIT;

								when "100" =>
									-- Selection 4
									m_steps <= x"000F4240";
									m_vlcty <= x"00007530";
									m_acc   <= x"00000FA0";
									m_drvI  <= "01011";
									state <= MOTOR_INIT;

								when "101" =>
									-- Selection 5
									m_steps <= x"000F4240";
									m_vlcty <= x"00007530";
									m_acc   <= x"00000FA0";
									m_drvI  <= "01011";
									state <= MOTOR_INIT;

								when "110" =>
									-- Selection 6
									m_steps <= x"000F4240";
									m_vlcty <= x"00007530";
									m_acc   <= x"00000FA0";
									m_drvI  <= "01011";
									state <= MOTOR_INIT;

								when "111" =>
									-- Selection 7
									m_steps <= x"000F4240";
									m_vlcty <= x"00007530";
									m_acc   <= x"00000FA0";
									m_drvI  <= "01011";
									state <= MOTOR_INIT;

								when others =>
									state <= IDLE;
							end case;

						else
							state <= IDLE;
						end if;

					when MOTOR_INIT =>
						-- Prepare the motor for motion
						inhibit  <= '0'; -- set inhibit bit to 0
						clr_done <= '1'; -- set clr_done bit to 1
						delay_counter <= '1';
						if (delay_counter = '1') then
							delay_counter <= '0';
							state    <= MOTOR_STROBE;
						end if;

					when MOTOR_STROBE =>
						-- Strobe the move bit and unstop
						move  <= '1'; -- set move bit to 1
						stop  <= '0'; -- set stop bit to 0
						state <= MOTOR_DRIVE;

					when MOTOR_DRIVE =>
						-- Drive the motor until done
						move <= '0'; -- unstrobe the move bit
						if (motor_done = '1') then
							state <= MOTOR_CLEAR;
						end if;

					when MOTOR_CLEAR =>
						-- Clear the motor and prepare for next drive
						inhibit  <= '1'; -- set inhibit bit to 1
						clr_done <= '0'; -- set clr_done bit to 0
						stop     <= '1'; -- set stop bit to 1
						state    <= IDLE;

				end case;
			end if;
		end if;
	end process;

	-- Hold module in reset with switch
	process (clk)
	begin
		if (rising_edge(clk)) then
			if (reset = '1' or user_switch = '0') then
				-- Main Reset
				mod_reset <= '1';
			else
				mod_reset <= '0';
			end if;
		end if;
	end process;

end architecture;
