module stepper_wrapper (
// Module signals
input clk
,input rst_n
,input enable

,input master_clr
,input hlim
,input llim
,input done_tmc2660_config
,output start_tmc2660_config
,output drvI_tmc2660_config
,output drv_inhibit
// General registers and control
,input [31:0] control
,input [31:0] expert
,output [9:0] status

// Control and Status registers for submodules
,input [31:0] acc   //motor acceleration
,input [31:0] vlcty //motor velocity
,input [31:0] steps //stepsto be moved
,input [4:0] drvI  //motor drive current

// NEW REGISTERS
,input [31:0] sub_stp
,output [31:0] steps_actual
,output [31:0] lacc
,output [31:0] lvlcty  //last motor velocity
,output [31:0] lsteps  //last steps to be moved
,output [31:0] sgn_steps //Signed steps
,output [31:0] abs_steps //Absolute steps

// Internal registers brought to top
,output [26:0] period
,output [26:0] clkrate
,output [19:0] accum

// Device IO
,output dir
,output step

);

reg isa_done;
wire move_done;

// High and Low frequency end switches are represented with Limit A/B
// Different cavities can have different wiring for the end-switches therefore
// limit switches are named ambigous
reg limit_a;
reg limit_b;

reg [2:0] move_buf;
wire motor_move;
reg motor_dir;
wire ldir;

reg [2:0] stop_buf;
wire motor_stop_in;
wire motor_stop;
// CONTROL BITS
wire ctl_move;
wire ctl_inhibit;
wire ctl_stop;
wire ctl_clr_done;
wire ctl_dir;
wire ctl_clr_sgn_step;
wire ctl_clr_abs_step;
wire ctl_en_sub_sgn;
wire ctl_en_sub_abs;

// STATUS BITS
wire stat_move_done;
wire stat_moving;

// CONTROL BITS
assign ctl_move         = control[0];
assign ctl_inhibit      = control[2];
assign ctl_stop         = control[3];
assign ctl_clr_done     = control[4];
assign ctl_dir          = control[5];
assign ctl_clr_sgn_step = !control[8];
assign ctl_clr_abs_step = !control[9];
assign ctl_en_sub_sgn   = control[10];
assign ctl_en_sub_abs   = control[11];
assign ctl_swap_lim     = expert[13];  // Used for swapping hlim and llim

stepper_driver driver(.clock(clk), .reset(rst_n),
    .steps_in(steps), .direction(motor_dir), .move(motor_move), .done_isa(isa_done),
    .accel_in(acc), .vlcty_in(vlcty), .stop(motor_stop),
    .clr_sgn_step(ctl_clr_sgn_step), .clr_abs_step(ctl_clr_abs_step),
    .en_sub_sgn(ctl_en_sub_sgn), .en_sub_abs(ctl_en_sub_abs),
    .sub_stp(sub_stp[15:0]),
    .done_tmc2660_config(done_tmc2660_config), .start_tmc2660_config(start_tmc2660_config),
    .drvi_tmc2660_config(drvI_tmc2660_config), .drv_inhibit(drv_inhibit),
    .step(step), .dir(ldir), .motion_led(stat_moving),
    .done_move(stat_move_done), .step_count_out(steps_actual),
    .laccel(lacc),.lvlcty(lvlcty),.lsteps(lsteps),
    .sgn_step(sgn_steps), .abs_step(abs_steps),
    .accum(accum), .clkrate(clkrate));

assign period = 0;  // no longer used
assign dir = ldir;

always@(posedge(clk)) begin : limit_ctl
	if(master_clr == 1'b1) begin
		limit_b <= 1'b0;
		limit_a <= 1'b0;
	end else begin
		limit_b <= ctl_swap_lim ? hlim : llim;
		limit_a <= ctl_swap_lim ? llim : hlim;
	end
end

always@(posedge(clk)) move_buf[0] <= ctl_move;
always@(posedge(clk)) move_buf[1] <= move_buf[0];
always@(posedge(clk)) move_buf[2] <= move_buf[1];

assign motor_move = (! move_buf[2]) & move_buf[1];

always@(posedge(clk)) stop_buf[0] <= ctl_stop;
always@(posedge(clk)) stop_buf[1] <= stop_buf[0];
always@(posedge(clk)) stop_buf[2] <= stop_buf[1];

assign motor_stop_in = (! stop_buf[2]) & stop_buf[1];
assign motor_stop = (motor_stop_in == 1'b1) || (ctl_inhibit == 1'b1) || (master_clr == 1'b1) || (limit_b & ldir) || (limit_a & !ldir) || (steps == 32'b0) || (vlcty == 32'b0) || (acc == 32'b0) || (drvI == 5'b0);

always@(posedge(clk)) begin : dir_ctl
//      if(master_clr == 1'b1) begin
//              motor_dir <= 1'b0;
//      end
	if(stat_moving == 1'b1) begin
		motor_dir <= motor_dir;
	end else begin
		motor_dir <= ctl_dir;
	end
end

assign move_done = (stat_move_done == 1'b1) || (motor_stop == 1'b1) || (limit_b & ldir) || (limit_a & !ldir);

always@(posedge(clk)) begin : isa_done_ctl
	if (ctl_clr_done == 1'b1) begin
		isa_done <= 1'b0;
	end
	if (move_done == 1'b1) begin
		isa_done <= 1'b1;
	end
end

// STATUS BITS
// Some of these are registered to help with fitting due to internal combinatorics
reg stat_moving_d = 1'b0;
always@(posedge(clk)) begin
    stat_moving_d <= stat_moving;
end
assign status = {{5{1'b0}}, limit_b, limit_a, isa_done, ldir, stat_moving_d};

endmodule
