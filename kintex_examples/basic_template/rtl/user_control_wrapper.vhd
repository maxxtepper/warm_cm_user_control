library ieee;
use ieee.std_logic_1164.all;

entity user_control_wrapper is
	port (
				 clock :in  std_logic;
				 reset :in  std_logic;

				 -- For controlling this module
				 en_in        :in std_logic;
				 rotary       :in std_logic_vector(7 downto 0);
				 left_button  :in std_logic;
				 right_button :in std_logic;

				 -- For driving the stepper
				 en_out    :out std_logic;
				 direction :out std_logic;
				 step_mode :out std_logic_vector(2 downto 0)
			 );

end user_control_wrapper;

architecture rtl of user_control_wrapper is

	-- Top FSM States
	type state_type is (IDLE, READY, LEFT, RIGHT);
	signal state : state_type;

	-- Module Reset
	signal mod_reset :std_logic;

	-- Switch and Buttons
	signal m_en_out       :std_logic;
	signal m_left_button  :std_logic;
	signal m_right_button :std_logic;
	--	Constants
	constant COUNT_MAX :integer   := 20;
	constant PULSE_MAX :integer   := 1;
	constant BTN_ACTIVE  :std_logic := '1';

	signal m_direction :std_logic;
	signal m_step_mode :std_logic_vector(2 downto 0);

begin

	direction <= m_direction;
	step_mode <= m_step_mode;

	-- Instance of Switch Enable for on/off
	i_switchEnable: entity work.switch_enable(rtl)
	port map(
						clock => clock,
						reset => reset,
						input => en_in,
						output => m_en_out
					);

	-- Instantiate Debounce for left button
	inst_leftDebouncer: entity work.debounce(rtl)
	generic map(
							 COUNT_MAX => COUNT_MAX,
							 PULSE_MAX => PULSE_MAX,
							 BTN_ACTIVE => BTN_ACTIVE)
	port map(
						clock => clock,
						reset => reset,
						button_in => left_button,
						pulse_out => m_left_button
					);

	-- Instantiate Debounce for right button
	inst_rightDebouncer: entity work.debounce(rtl)
	generic map(
							 COUNT_MAX => COUNT_MAX,
							 PULSE_MAX => PULSE_MAX,
							 BTN_ACTIVE => BTN_ACTIVE)
	port map(
						clock => clock,
						reset => reset,
						button_in => right_button,
						pulse_out => m_right_button
					);

	-- Instance of Rotary Encoder
	i_RotaryEncoder: entity work.rotary_encoder(rtl)
	port map(
						clock => clock,
						reset => reset,
						input => rotary,
						output => m_step_mode
					);

	-- Top FSM
	process(clock)
	begin
		if (rising_edge(clock)) then
			if (mod_reset = '1') then
				-- Main Reset
				state <= IDLE;

			else
				case state is
					when IDLE =>
						if (en_in = '1') then
							state <= READY;
						end if;

					when READY =>
						-- Stepper is ready to move
						if (m_direction = '1') then
							state <= LEFT;
						elsif (m_direction = '0') then 
							state <= RIGHT;
						end if;

					when LEFT =>
						if (m_direction /= '1') then
							-- button is not pressed, back to ready
							state <= READY;
						end if;

					when RIGHT =>
						if (m_direction /= '0') then
							-- button is not pressed, back to ready
							state <= READY;
						end if;

					when others =>
						state <= IDLE;

				end case;
			end if;
		end if;
	end process;

	-- Hold module in reset with switch
	process (clock)
	begin
		if (rising_edge(clock)) then
			if (reset = '1' or en_in = '0') then
				-- Main Reset
				mod_reset <= '1';
			else
				mod_reset <= '0';
			end if;
		end if;
	end process;

	-- Enable the stepper to run
	process (clock)
	begin
		if (rising_edge(clock)) then
			if (mod_reset = '1') then
				-- Main Reset
				en_out <= '0';
			else
				-- This if checks for the switch, for a single button press, and for a valid rotary position
				if ((m_en_out = '1') and ((m_left_button xor m_right_button) = '1') and (m_step_mode /= "000")) then
					en_out <= '1';
				else
					en_out <= '0';
				end if;
			end if;
		end if;
	end process;

	-- Set direction based on buttons
	process (clock)
	begin
		if (rising_edge(clock)) then
			if (mod_reset = '1') then
				-- Main Reset
				m_direction <= '0';
			elsif (m_left_button = '0' and m_right_button = '0') then
				m_direction <= '0';
			elsif (m_left_button = '0' and m_right_button = '1') then
				m_direction <= '0';
			elsif (m_left_button = '1' and m_right_button = '0') then
				m_direction <= '1';
			elsif (m_left_button = '1' and m_right_button = '1') then
				m_direction <= '1';
			end if;
		end if;
	end process;

end architecture;
