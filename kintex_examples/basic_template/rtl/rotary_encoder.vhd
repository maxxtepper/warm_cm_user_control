library ieee;
use ieee.std_logic_1164.all;

entity rotary_encoder is
	port (
				 -- Local signals
				 clock : in  std_logic;
				 reset : in  std_logic;
				 input : in std_logic_vector(7 downto 0);
				 output : out std_logic_vector(2 downto 0)
			 );
end rotary_encoder;

architecture rtl of rotary_encoder is

begin

	process(clock)
	begin
		if rising_edge(clock) then
			if reset = '1' then
				-- Disable output
				output <= (others => '0');

			else
				case input is
					when "11111110" => output <= "000";
					when "11111101" => output <= "001";
					when "11111011" => output <= "010";
					when "11110111" => output <= "011";
					when "11101111" => output <= "100";
					when "11011111" => output <= "101";
					when "10111111" => output <= "110";
					when "01111111" => output <= "111";
					when others     => output <= "000";

				end case;
			end if;
		end if;
	end process;

end architecture;
