library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.components.all;

entity stepper_driver is
	port(reset                :in std_logic;
			 clock                :in std_logic;
			 steps_in             :in std_logic_vector(31 downto 0); --this is steps
			 direction            :in std_logic;
			 move                 :in std_logic; --this is go
			 done_isa             :in std_logic;
			 accel_in             :in std_logic_vector(31 downto 0); --this is acceleration
			 vlcty_in             :in std_logic_vector(31 downto 0); --this is velocity
			 stop                 :in std_logic;
			 clr_sgn_step         :in std_logic;
			 clr_abs_step         :in std_logic;
			 en_sub_abs           :in std_logic;
			 en_sub_sgn           :in std_logic;
			 sub_stp              :in std_logic_vector(15 downto 0);
			 done_tmc2660_config  :in std_logic;
			 start_tmc2660_config :out std_logic;
			 drvi_tmc2660_config  :out std_logic;
			 drv_inhibit          :out std_logic;
			 step                 :out std_logic; --this is step_out
			 dir                  :out std_logic; --this is direction_out
			 motion_led           :out std_logic;
			 done_move            :out std_logic;
			 step_count_out       :out std_logic_vector(31 downto 0);
			 laccel               :out std_logic_Vector(31 downto 0);
			 lvlcty               :out std_logic_Vector(31 downto 0);
			 lsteps               :out std_logic_Vector(31 downto 0);
			 sgn_step             :out std_logic_vector(31 downto 0);
			 abs_step             :out std_logic_vector(31 downto 0);
			 accum                :out std_logic_vector(19 downto 0); --this is denominator
			 clkrate              :out std_logic_vector(26 downto 0) --this is numerator
		 );
end entity stepper_driver;

architecture behavior of stepper_driver is

	signal clkrate_i            :std_logic_vector(26 downto 0);
	signal accum_i              :std_logic_vector(19 downto 0);
	signal en_accel             :std_logic;
	signal accel                :std_logic_vector(31 downto 0);
	signal en_vlcty             :std_logic;
	signal vlcty                :std_logic_vector(31 downto 0);
	signal en_steps             :std_logic;
	signal steps                :std_logic_vector(31 downto 0);
	signal abs_step_int         :std_logic_vector(31 downto 0);
	signal en_abs_steps         :std_logic;
	--signal clr_abs_steps      :std_logic;
	--signal clr_sgn_step       :std_logic;
	signal en_sgn_steps         :std_logic;
	signal sgn_step_in          :std_logic_vector(31 downto 0);
	--signal sgn_step           :std_logic_vector(31 downto 0);
	signal motion_led_int       :std_logic;
	signal en_dir               :std_logic;
	signal clr_accum_accel      :std_logic;
	signal en_accum_accel       :std_logic;
	signal en_accum_accel_temp  :std_logic;
	signal accum_accel_in       :std_logic_vector(31 downto 0);
	signal accum_accel          :std_logic_vector(31 downto 0);
	signal clr_ramp_steps       :std_logic;
	signal en_ramp_steps        :std_logic;
	signal ramp_steps           :std_logic_vector(31 downto 0);
	signal clr_step_count       :std_logic;
	signal en_step_count        :std_logic;
	signal step_count           :std_logic_vector(31 downto 0);
	signal clr_step_count_isa   :std_logic;
	signal en_step_count_isa    :std_logic;
	signal step_count_isa_in    :std_logic_vector(31 downto 0);
	signal clr_period_count     :std_logic;
	signal en_period_count      :std_logic;
	signal period_count         :std_logic_vector(31 downto 0);
	signal period_xcount        :std_logic_vector(31 downto 0);
	signal half_steps           :std_logic_vector(31 downto 0);
	signal step_odd             :std_logic_vector(31 downto 0);
	signal step_temp            :std_logic_vector(31 downto 0);
	signal temp1                :std_logic_vector(31 downto 0);
	signal temp2                :std_logic_vector(31 downto 0);
	signal accum_accel_temp     :std_logic_vector(31 downto 0);
	signal sel1                 :std_logic;
	signal sel2                 :std_logic;
	signal temp_num             :std_logic_vector(31 downto 0);
	signal rst_done_move        :std_logic;
	signal en_done_move         :std_logic;
	signal done_buf             :std_logic;
	signal done_buf1            :std_logic;
	signal done_buf2            :std_logic;
	signal done_pulse           :std_logic;
	signal pulse_on_perd        :std_logic;
	signal pulse_perd           :std_logic;
	signal steps_end            :std_logic;
	signal holding              :std_logic;
	signal deceling             :std_logic;
	signal acceling             :std_logic;
	signal ramp_steps_zero      :std_logic;
	signal count_half_step_even :std_logic;
	signal count_half_step_odd  :std_logic;
	signal abs_step_in          :std_logic_vector(31 downto 0);
	signal abs_step_in_int      :std_logic_vector(31 downto 0);
	signal sgn_step_in_int1     :std_logic_vector(31 downto 0);
	signal sgn_step_in_int2     :std_logic_vector(31 downto 0);
	signal sgn_stp_dir          :std_logic_vector(31 downto 0);
	signal sgn_step_int         :std_logic_vector(31 downto 0);

	type state_type is (INIT, START_CONFIG, WAIT_CONFIG_DONE, LOAD, PULSE_ON, PULSE_OFF, START_DRVI0, WAIT_DRVI0_DONE);
	signal STATE							: state_type;

	----------end of signals for motor-------------------------

	signal one   :std_logic;
	signal zeros :std_logic_vector(31 downto 0);

begin
	one		<= '1';
	zeros		<= (others => '0');

	clkrate_i					<=  "010" & x"FAF080"; -----this is 50,000,000 clock cycles @ 50MHz = 1 sec, dependent on frequency of operation
	clkrate					<= clkrate_i;
	accum_i							<= vlcty(19 downto 0) when holding = '1' else accum_accel(19 downto 0);
	accum					<= accum_i;
	pulse_on_perd				<= '1' when period_count = x"000000C7" else '0'; ---200 clock cycles @50 MHz = 200 * 20 ns = 4 us
	pulse_perd					<= '1' when period_xcount >= clkrate_i - 1 else '0';
	steps_end					<= '1' when step_count = steps - 1 else '0';
	holding						<= '1' when accum_accel >= vlcty else '0';
	deceling						<= '1' when step_count  > (steps - ramp_steps - "10") else '0';
	acceling						<= '1' when (step_count + 1)  < half_steps else '0';
	ramp_steps_zero			<= '1' when ramp_steps = zeros else '0';
	count_half_step_even		<= '1' when (step_count + 1 = half_steps and steps(0) = '0') else '0';
	count_half_step_odd		<= '1' when (step_count + 1 = half_steps and steps(0) = '1') else '0';

	done_buf_i: latch_n
	port map(clock 	=> clock,
					 reset 	=> reset,
					 clear	=> one,
					 en 		=> one,
					 inp 	=> done_isa,
					 oup 	=> done_buf
				 );

	done_buf1_i: latch_n
	port map(clock 	=> clock,
					 reset 	=> reset,
					 clear	=> one,
					 en 		=> one,
					 inp 	=> done_buf,
					 oup 	=> done_buf1
				 );

	done_buf2_i: latch_n
	port map(clock 	=> clock,
					 reset 	=> reset,
					 clear	=> one,
					 en 		=> one,
					 inp 	=> done_buf1,
					 oup 	=> done_buf2
				 );

	done_pulse <= done_buf2 and not done_buf1;

	accel_reg: REGNE generic map(N => 32)
	port map (CLOCK	=> clock,
						RESET	=> reset,
						CLEAR	=> '1',
						EN  	=> en_accel, ---this value of acceleration gets loaded in LOAD state
						INPUT 	=> accel_in,
						OUTPUT 	=> accel
					);

	laccel	<= accel;

	vlcty_reg: REGNE generic map(N => 32)
	port map (CLOCK	=> clock,
						RESET	=> reset,
						CLEAR	=> '1',
						EN  	=> en_vlcty, ---this value of velocity gets loaded in LOAD state
						INPUT 	=> vlcty_in,
						OUTPUT 	=> vlcty
					);

	lvlcty	<= vlcty;
	steps_reg: REGNE generic map(N => 32)
	port map (CLOCK	=> clock,
						RESET	=> reset,
						CLEAR	=> '1',
						EN 	=> en_steps, ---this value of steps gets loaded in LOAD state
						INPUT  	=> steps_in,
						OUTPUT 	=> steps
					);

	lsteps	<= steps;
	dir_ff: latch_n port map(clock 	=> clock,
													 reset 	=> reset,
													 clear	=> one,
													 en 		=> one,
													 inp 	=> direction,
													 oup 	=> dir
												 );

	en_accel		<= '1' when STATE = LOAD else '0';
	en_vlcty		<= '1' when STATE = LOAD else '0';
	en_steps		<= '1' when STATE = LOAD else '0';
	en_dir			<= '1' when STATE = LOAD else '0';
	temp1					<= accum_accel + accel;
	temp_num				<= accum_accel - accel;
	temp2					<= temp_num when (accum_accel >  accel) else zeros;
	accum_accel_temp	<= temp2 when sel1 = '1' else temp1;
	accum_accel_in		<= accel when sel2 = '1' else accum_accel_temp;
	accum_accel_reg: REGNE generic map(N => 32)

	port map (CLOCK	 => clock,
						RESET	 => reset,
						CLEAR	 => clr_accum_accel,
						EN 	 => en_accum_accel,
						INPUT  	 => accum_accel_in,
						OUTPUT 	 => accum_accel
					);

	accum_accel_en_ff: latch_n port map(clock 	=> clock,
																			reset 	=> reset,
																			clear	=> one,
																			en 		=> one,
																			inp 	=> en_accel,
																			oup 	=> en_accum_accel_temp
																		);

	step_odd			<= steps + 1;--step_motor1_odd
	step_temp		<= step_odd when (steps(0) = '1') else steps; --step_temp_motor
	half_steps		<= '0' & step_temp(31 downto 1); --half_step_motor

	ramp_steps_count_reg: REGNE generic map(N => 32)
	port map (CLOCK	 => clock,
						RESET	 => reset,
						CLEAR	 => clr_ramp_steps,
						EN   	 => en_ramp_steps,
						INPUT    => step_count,
						OUTPUT   => ramp_steps
					);

	step_counter: COUNTER generic map(N=> 32) ---counter for tracking steps
	port map(CLOCK	=> clock,
					 RESET	=> reset,
					 CLEAR	=> clr_step_count,
					 ENABLE	=> en_step_count,
					 COUNT	=> step_count
				 );

	step_counter_reg_isa: REGNE generic map(N => 32)
	port map (CLOCK  	=> clock,
						RESET	=> reset,
						CLEAR	=> clr_step_count_isa,
						EN	=> en_step_count_isa,
						INPUT   => step_count_isa_in,
						OUTPUT  => step_count_out
					);

	step_count_isa_in		<= step_count + 1;
	en_step_count_isa		<= '0' when STATE = INIT or STATE = LOAD else '1';
	clr_step_count_isa	<= '0' when (move = '1' or done_pulse = '1') else '1';

-- new hand-drawn accumulator, in addition to COUNTER instantiation below that only adds 1 per cycle
process(clock, reset) begin
	if (reset = '0') then
		period_xcount <= (OTHERS => '0');
	elsif (clock = '1' and clock'EVENT) then
		if (clr_period_count = '0') then
			period_xcount <= (OTHERS => '0');
		elsif (en_period_count = '1') then
			period_xcount <= period_xcount + accum_i;
		end if;
	end if;
end process;

period_counter: COUNTER generic map(N=> 32) ---counter for tracking the period of the pulse
port map(CLOCK	=> clock,
				 RESET	=> reset,
				 CLEAR	=> clr_period_count,
				 ENABLE	=> en_period_count,
				 COUNT	=> period_count
			 );

abs_step_in_int <= 	abs_step_int - (x"0000" & sub_stp);
abs_step_in 	<= 	abs_step_in_int when en_sub_abs = '1' and  abs_step_in_int(31) = '0' else
								 x"00000000" when en_sub_abs = '1' and  abs_step_in_int(31) = '1' else
								 abs_step_int + '1';

abs_step_reg_0: REGNE generic map(N => 32) ---tracking total number of steps
port map (CLOCK  	=> clock,
					RESET	=> reset,
					CLEAR	=> clr_abs_step,
					EN	=> en_abs_steps,
					INPUT   => abs_step_in,
					OUTPUT  => abs_step_int
				);

abs_step <= abs_step_int;

sgn_step_reg_0: REGNE generic map(N => 32)
port map (CLOCK  	=> clock,
					RESET	=> reset,
					CLEAR	=> clr_sgn_step,
					EN	=> en_sgn_steps,
					INPUT   => sgn_step_in,
					OUTPUT  => sgn_step_int
				);

sgn_step <= sgn_step_int;
sgn_step_in_int1 <= sgn_step_int - (x"0000" & sub_stp);
sgn_step_in_int2 <= sgn_step_int + (x"0000" & sub_stp);
sgn_step_in <= 	sgn_step_in_int1 when (en_sub_sgn = '1' and sgn_step_in_int1(31) = '0' and sgn_step_int(31) = '0') else
								x"00000000" when (en_sub_sgn = '1' and sgn_step_in_int1(31) = '1' and sgn_step_int(31) = '0') else
								sgn_step_in_int2 when (en_sub_sgn = '1' and sgn_step_in_int2(31) = '1' and sgn_step_int(31) = '1') else
								x"00000000" when (en_sub_sgn = '1' and sgn_step_in_int2(31) = '0' and sgn_step_int(31) = '1') else
								sgn_step_int + '1' when direction = '0'  and motion_led_int = '1' else
								sgn_step_int - '1' when direction = '1' and motion_led_int = '1' else
								x"00000000";

process(clock, reset, stop)
begin
	if(reset = '0' or stop = '1') then
		STATE <= INIT;
	elsif(clock = '1' and clock'event) then
		case STATE is

			when INIT		=> 	
				if done_buf2 = '0' then ---if the done flag from previous operation has been cleared, check for move command
					if(move = '1') then 
						STATE <= START_CONFIG; ---if move command is issued, load all the values
					else STATE <= INIT; ---if there is no move command wait for the command
					end if;
				else STATE <= INIT; ---if the done flag hasn't been cleared, wait in INIT state for it to be cleared
				end if;

			when START_CONFIG	=>	
				STATE <= WAIT_CONFIG_DONE;

			when WAIT_CONFIG_DONE	=> 	
				if done_tmc2660_config = '1' then 
					STATE <= LOAD;
				else 
					STATE <= WAIT_CONFIG_DONE;
				end if;

			when LOAD		=>	
				STATE <= PULSE_ON;

			when PULSE_ON		=>	
				if(pulse_on_perd = '1') then 
					STATE <= PULSE_OFF; ---if pulse is on for 250 ns go to PULSE_OFF
				else 
					STATE <= PULSE_ON; ---if pulse hasn't been high for 250 ns stay in the same state
				end if;

			when PULSE_OFF		=>	
				if(pulse_perd = '1') then
					if(steps_end = '1') then 
						STATE <= START_DRVI0; ---if steps end go to INIT state
					else 
						STATE <= PULSE_ON; ---else go to PULSE_ON state
					end if;
				else 
					STATE <= PULSE_OFF; ---if pulse period is not completed staty in the same state
				end if;

			when START_DRVI0	=> 	
				STATE <= WAIT_DRVI0_DONE;

			when WAIT_DRVI0_DONE	=> 	
				if done_tmc2660_config = '1' then 
					STATE <= INIT;
				else 
					STATE <= WAIT_DRVI0_DONE;
				end if;

			when OTHERS		=> 	STATE <= INIT;

		end case;
	end if;
end process;

en_period_count			<= '1' when (STATE = PULSE_ON) or (STATE = PULSE_OFF) else '0';
en_step_count			<= '1' when (STATE = PULSE_OFF and pulse_perd = '1' and steps_end = '0') else '0';
en_accum_accel			<= '1' when (en_accum_accel_temp = '1') or (STATE = PULSE_OFF and pulse_perd = '1' and steps_end = '0' and ((holding = '0' and count_half_step_even = '0') or 							(holding = '1' and (deceling = '1' or (ramp_steps_zero = '1' and count_half_step_odd = '1'))))) else '0';
en_ramp_steps			<= '1' when (STATE = PULSE_OFF and pulse_perd = '1' and steps_end = '0' and holding = '1' and ramp_steps_zero = '1') else '0';
sel1				<= '0' when (STATE = PULSE_OFF and pulse_perd = '1' and steps_end = '0' and holding = '0' and acceling = '1') else '1';
sel2				<= '1' when (en_accum_accel_temp = '1') else '0';
clr_step_count			<= '0' when (move = '1') else '1';
clr_period_count		<= '0' when (move = '1') or (STATE = PULSE_OFF and pulse_perd = '1') or (stop = '1') else '1';
clr_accum_accel			<= '0' when (stop = '1') else '1';
clr_ramp_steps			<= '0' when (STATE = LOAD) or (STATE = PULSE_OFF and pulse_perd = '1' and steps_end = '1') or (stop = '1') else '1';
step				<= '1' when (STATE = PULSE_ON) else '0';
done_move			<= '1' when (STATE = PULSE_OFF and pulse_perd = '1' and steps_end = '1') else '0';
motion_led_int			<= '1' when (STATE = LOAD ) or (STATE = PULSE_ON) or (STATE = PULSE_OFF) else '0';
en_sgn_steps			<= '1' when (en_sub_sgn = '1') or (STATE = PULSE_ON and pulse_on_perd = '1') else '0';
en_abs_steps 			<= '1' when (en_sub_abs = '1') or (STATE = PULSE_ON and pulse_on_perd = '1') else '0';
motion_led			<= motion_led_int;
start_tmc2660_config		<= '1' when (STATE = START_CONFIG) or (STATE = START_DRVI0) else '0';
drvi_tmc2660_config		<= '1' WHEN (STATE = START_CONFIG) or (STATE = WAIT_CONFIG_DONE) ELSE '0';
drv_inhibit			<= '0' WHEN (STATE = LOAD) or (STATE = PULSE_ON) or (STATE = PULSE_OFF) ELSE '1';

end architecture behavior;
