library ieee;
use ieee.std_logic_1164.all;

entity switch_enable is
	port (
				 -- Local signals
				 clock : in  std_logic;
				 reset : in  std_logic;
				 input : in std_logic;
				 output : out std_logic
			 );
end switch_enable;

architecture rtl of switch_enable is

begin

	process(clock)
	begin
		if rising_edge(clock) then
			if reset = '1' then
				-- Disable output
				output <= '0';

			else
				-- Input -> Output
				if input = '1' then
					output <= '1';
				else
					output <= '0';
				end if;

			end if;
		end if;
	end process;

end architecture;
