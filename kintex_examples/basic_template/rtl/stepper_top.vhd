library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity stepper_top is
	generic(
				constant ZERO :std_logic := '0';
				constant ONE  :std_logic := '1'
			);
	port(
				-- Global signals
				clk :in std_logic;

				-- Interlock permit (not sure if I need this)
				interlock_permit :in std_logic;

				-- Motor signals to off-board devices
				m_hflf :in std_logic; -- high frequency limit fault
				m_lflf :in std_logic; -- low frequency limit fault
				m_sdo  :in std_logic; -- serial data out from the driver
				m_sda  :in std_logic; -- serial data i/o from temp sensor on the stepper motor board
				m_step :out std_logic; -- step input to the driver
				m_dir  :out std_logic; -- direction input to the driver
				m_en   :out std_logic; -- enable signal for the mosfets on the stepper driver
				m_sdi  :out std_logic; -- serial data input to the driver
				m_sclk :out std_logic; -- serial input clock to the driver
				m_csn  :out std_logic; -- chip select input to the driver
				m_scl  :out std_logic; -- serial clock input to the temperature sensor on the stepper motor board

				m_cntl :in std_logic_vector(31 downto 0);
				-- bf : m_cntl
				-- 0 : move
				-- 2 : inhibit
				-- 3 : stop
				-- 4 : clr_done
				-- 8 : clr_sgn_step
				-- 9 : clr_abs_step
				-- 10 : en_sub_sgn
				-- 11 : en_sub_abs

				m_expert :in std_logic_vector(31 downto 0); -- external, bitfield
				-- bf : m_expert
				-- 13 : en_sub_abs

				m_acc          :in std_logic_vector(31 downto 0); -- external, motor acceleration
				m_vlcty        :in std_logic_vector(31 downto 0); -- external, motor velocity
				m_steps        :in std_logic_vector(31 downto 0); -- external, stepsto be moved
				m_sub_stp      :in std_logic_vector(31 downto 0); -- external

				m_steps_actual :out std_logic_vector(31 downto 0); -- eventually external
				m_period       :out std_logic_vector(26 downto 0); -- eventually external
				m_clkrate      :out std_logic_vector(26 downto 0); -- eventually external
				m_accum        :out std_logic_vector(19 downto 0); -- eventually external
				m_lacc         :out std_logic_vector(31 downto 0); -- eventually external
				m_lvlcty       :out std_logic_vector(31 downto 0); -- eventually external, last motor velocity
				m_lsteps       :out std_logic_vector(31 downto 0); -- eventually external, last steps to be moved
				m_sgn_steps    :out std_logic_vector(31 downto 0); -- eventually external, Signed steps
				m_abs_steps    :out std_logic_vector(31 downto 0); -- eventually external, Absolute steps
				m_status       :out std_logic_vector(31 downto 0); -- eventually external
				-- bf : m_status
				-- 0 : moving
				-- 2 : move_done

				m_drvI :in std_logic_vector(4 downto 0); -- external

				master_clr    :in std_logic;
				mconfig_done  :inout std_logic;
				mstart_config :inout std_logic;
				mdrvI_config  :inout std_logic;
				mdrv_inhibit  :inout std_logic

			);
end entity stepper_top;

architecture rtl of stepper_top is

begin
	-- TMC2660 test
	inst_stepperTest : entity work.tmc2660(behavior)
	port map(
						CLOCK        => clk,
						RESET        => ONE,
						DRVI         => m_drvI,
						DRVI_CONFIG  => mdrvI_config,
						INHIBIT      => mdrv_inhibit,
						START_CONFIG => mstart_config,
						SDO          => m_sdo,
						CSN          => m_csn,
						SCK          => m_sclk,
						SDI          => m_sdi,
						SEN          => m_en,
						CONFIG_DONE  => mconfig_done
					);

	m_scl <= 'Z';

	-- Instance of stepper_wrapper
	inst_stepperWrapper : entity work.stepper_wrapper(rtl)
	port map(
						-- Module signals
						clk                  => clk,
						rst_n                => ONE,
						enable               => ZERO,
						master_clr           => ZERO,
						hlim                 => m_hflf,
						llim                 => m_lflf,
						done_tmc2660_config  => mconfig_done,
						start_tmc2660_config => mstart_config,
						drvI_tmc2660_config  => mdrvI_config,
						drv_inhibit          => mdrv_inhibit,

						-- General registers and control
						control => m_cntl,
						expert  => m_expert,
						status  => m_status(9 downto 0),

						-- Control and Status registers for submodules
						acc   => m_acc, --motor acceleration
						vlcty => m_vlcty, --motor velocity
						steps => m_steps, --stepsto be moved
						drvI  => m_drvI, --motor drive current

						-- NEW REGISTERS
						sub_stp      => m_sub_stp,
						steps_actual => m_steps_actual,
						lacc         => m_lacc,
						lvlcty       => m_lvlcty, --last motor velocity
						lsteps       => m_lsteps, --last steps to be moved
						sgn_steps    => m_sgn_steps, --Signed steps
						abs_steps    => m_abs_steps, --Absolute steps

						-- Internal registers brought to top
						period  => m_period,
						clkrate => m_clkrate,
						accum   => m_accum,

						-- Device IO
						step => m_step,
						dir  => m_dir
					);

end architecture;
