library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debounce is
	generic(
	constant COUNT_MAX :integer := 20; -- clocks needed for a pulse
	constant PULSE_MAX :integer := 20; -- clocks for the pulse
	constant BTN_ACTIVE :std_logic := '1'); -- active high or low
	port(
				clock :in std_logic;
				reset :in std_logic;
				button_in :in std_logic;
				pulse_out :out std_logic
			);

end debounce;

architecture rtl of debounce is

	--	Counters
	signal wait_count  :integer := 0;
	signal pulse_count :integer := 0;
	
	--	FSM States
	type state_type is (IDLE, WAIT_TIME, PULSE_HOLD);
	signal state :state_type := IDLE;

begin

	--	FSM
	process(reset,clock)
	begin
		if (reset = '1') then
			state <= IDLE;
			pulse_out <= '0';
		elsif (rising_edge(clock)) then

			case (state) is
				when IDLE =>
					if (button_in = BTN_ACTIVE) then
						state <= WAIT_TIME;
					else
						state <= IDLE;
					end if;
					pulse_out <= '0';

				when WAIT_TIME =>
					if (wait_count = COUNT_MAX) then
						wait_count <= 0;
						if (button_in = BTN_ACTIVE) then
							pulse_out <= '1';
							state <= PULSE_HOLD;
						end if;
					else
						wait_count <= wait_count + 1;
					end if;

				when PULSE_HOLD =>
					if (pulse_count = PULSE_MAX) then
						pulse_count <= 0;
						pulse_out <= '0';
						state <= IDLE;
					else
						pulse_count <= pulse_count + 1;
					end if;

			end case;
		end if;
	end process;

end architecture;

