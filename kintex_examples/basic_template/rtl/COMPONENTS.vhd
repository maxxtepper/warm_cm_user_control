LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;


PACKAGE COMPONENTS IS

TYPE REG17_3 IS ARRAY(3 DOWNTO 0) OF STD_LOGIC_VECTOR(16 DOWNTO 0);
TYPE REG16_3 IS ARRAY(2 DOWNTO 0) OF STD_LOGIC_VECTOR(15 DOWNTO 0);
TYPE REG16_4 IS ARRAY(3 DOWNTO 0) OF STD_LOGIC_VECTOR(15 DOWNTO 0);
TYPE REG16_5 IS ARRAY(4 DOWNTO 0) OF STD_LOGIC_VECTOR(15 DOWNTO 0);
TYPE REG16_6 IS ARRAY(5 DOWNTO 0) OF STD_LOGIC_VECTOR(15 DOWNTO 0);
TYPE REG16_8 IS ARRAY(7 DOWNTO 0) OF STD_LOGIC_VECTOR(15 DOWNTO 0);
TYPE REG16_9 IS ARRAY(8 DOWNTO 0) OF STD_LOGIC_VECTOR(15 DOWNTO 0);
TYPE REG16_14 IS ARRAY(13 DOWNTO 0) OF STD_LOGIC_VECTOR(15 DOWNTO 0);
TYPE REG16_18 IS ARRAY(17 DOWNTO 0) OF STD_LOGIC_VECTOR(15 DOWNTO 0);
TYPE REG18_2 IS ARRAY(1 DOWNTO 0) OF STD_LOGIC_VECTOR(17 DOWNTO 0);
TYPE REG18_3 IS ARRAY(2 DOWNTO 0) OF STD_LOGIC_VECTOR(17 DOWNTO 0);
TYPE REG24_8 IS ARRAY(7 DOWNTO 0) OF STD_LOGIC_VECTOR(23 DOWNTO 0);
TYPE REG24_16 IS ARRAY(15 DOWNTO 0) OF STD_LOGIC_VECTOR(23 DOWNTO 0);
TYPE REG32_4 IS ARRAY(3 DOWNTO 0) OF STD_LOGIC_VECTOR(31 DOWNTO 0);
TYPE FIR9_ARRAY IS ARRAY(16 DOWNTO 0) OF STD_LOGIC_VECTOR(8 DOWNTO 0);
TYPE FIR10_ARRAY IS ARRAY(16 DOWNTO 0) OF STD_LOGIC_VECTOR(9 DOWNTO 0);
TYPE FIR26_ARRAY IS ARRAY(16 DOWNTO 0) OF STD_LOGIC_VECTOR(25 DOWNTO 0);
TYPE FIR27_ARRAY IS ARRAY(16 DOWNTO 0) OF STD_LOGIC_VECTOR(26 DOWNTO 0);
TYPE FIR32_ARRAY IS ARRAY(16 DOWNTO 0) OF STD_LOGIC_VECTOR(31 DOWNTO 0);
TYPE REG18_ARRAY IS ARRAY(15 DOWNTO 0) OF STD_LOGIC_VECTOR(17 DOWNTO 0);
TYPE REG5_16ARRAY IS ARRAY(15 DOWNTO 0) OF INTEGER RANGE 0 TO 31;
TYPE REG10_16ARRAY IS ARRAY(9 DOWNTO 0) OF STD_LOGIC_VECTOR(15 DOWNTO 0);

COMPONENT ADDER IS
        GENERIC(N : INTEGER := 16);
        PORT(AIN        : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
              BIN       : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
              SUM       : OUT STD_LOGIC_VECTOR(N DOWNTO 0)
              );
END COMPONENT;

COMPONENT REGNE
        GENERIC(N : INTEGER := 16);
        PORT(CLOCK  : IN STD_LOGIC;
              RESET     : IN STD_LOGIC;
              CLEAR  : IN STD_LOGIC;
              EN        : IN STD_LOGIC;
              INPUT : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
              OUTPUT : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
            );
END COMPONENT;

COMPONENT COUNTER
        GENERIC(N : INTEGER := 5);
        PORT(CLOCK      : IN STD_LOGIC;
              RESET     : IN STD_LOGIC;
              CLEAR     : IN STD_LOGIC;
              ENABLE        : IN STD_LOGIC;
              COUNT     : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
             );
END COMPONENT;

COMPONENT SHIFT_REG IS

    GENERIC(N : INTEGER := 16);

    PORT(CLOCK : IN STD_LOGIC;
             RESET : IN STD_LOGIC;
         EN : IN STD_LOGIC;
         LOAD : IN STD_LOGIC;
         INP : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
         OUTPUT : OUT STD_LOGIC
         );
     END COMPONENT;

COMPONENT SHIFT_REG_CRC
        GENERIC (N :INTEGER:= 16);
    PORT (CLOCK : IN STD_LOGIC;
          RESET : IN STD_LOGIC;
          CLR   : IN STD_LOGIC;
          EN    : IN STD_LOGIC;
          INP   : IN STD_LOGIC;
          OUTPUT : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
             );
END COMPONENT;

COMPONENT SHIFT_LEFT_REG_AD7367 IS
    GENERIC(N : INTEGER := 16);
    PORT(CLOCK : IN STD_LOGIC;
         RESET : IN STD_LOGIC;
         EN : IN STD_LOGIC;
         INP : IN STD_LOGIC;
         OUTPUT : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
         );
END COMPONENT;

COMPONENT SHIFT_LEFT_REG_ADS8353 IS
    GENERIC(N : INTEGER := 16);
    PORT(CLOCK  : IN STD_LOGIC;
          RESET     : IN STD_LOGIC;
          EN        : IN STD_LOGIC;
          LOAD  : IN STD_LOGIC;
          INP   : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          OUTPUT : OUT STD_LOGIC
          );
END COMPONENT;

COMPONENT FLIP_FLOP IS
    PORT(CLOCK  : IN STD_LOGIC;
          RESET     : IN STD_LOGIC;
          CLEAR  : IN STD_LOGIC;
          EN        : IN STD_LOGIC;
          INP   : IN STD_LOGIC;
          OUP   : OUT STD_LOGIC
          );
END COMPONENT;

component latch_n is
    port(clock : in std_logic;
         reset : in std_logic;
         clear  : in std_logic;
         en : in std_logic;
         inp : in std_logic;
         oup : out std_logic
        );
end component;

COMPONENT SHIFT_LEFT_REG IS
    GENERIC(N : INTEGER := 16);
    PORT(CLOCK  : IN STD_LOGIC;
          RESET     : IN STD_LOGIC;
          EN        : IN STD_LOGIC;
          LOAD  : IN STD_LOGIC;
          INP   : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          OUTPUT : OUT STD_LOGIC
          );
END COMPONENT;

COMPONENT SHIFT_LEFT_REG_CRC IS
    GENERIC(N : INTEGER := 16);
    PORT(CLOCK  : IN STD_LOGIC;
          RESET     : IN STD_LOGIC;
          EN        : IN STD_LOGIC;
          CLEAR : IN STD_LOGIC;
          LOAD  : IN STD_LOGIC;
          INP   : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          OUTPUT : OUT STD_LOGIC
          );
END COMPONENT;
COMPONENT SHIFT_LEFT_BIT IS
    GENERIC(N : INTEGER := 16);
    PORT(CLOCK      : IN STD_LOGIC;
          RESET         : IN STD_LOGIC;
          EN            : IN STD_LOGIC;
          LOAD      : IN STD_LOGIC;
          INP       : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          SHIFT_BIT : IN STD_LOGIC;
          OUTPUT    : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
          );
END COMPONENT;

COMPONENT SHIFT_SQUARE_ROOT IS
    GENERIC(N : INTEGER := 64);
    PORT(CLOCK      : IN STD_LOGIC;
          RESET     : IN STD_LOGIC;
          CLEAR     : IN STD_LOGIC;
          EN        : IN STD_LOGIC;
          LOAD      : IN STD_LOGIC;
          INP       : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          SHIFT_IN  : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
          OUTPUT    : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          SHIFT_BIT : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
          );
END COMPONENT;

COMPONENT SHIFT_BY_M IS
    GENERIC(N : INTEGER := 18);
    PORT(D_IN   : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
         M      : IN     INTEGER RANGE 0 TO 31;
         D_OUT  : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
          );
END COMPONENT SHIFT_BY_M;

COMPONENT MP2IQ_INIT IS

PORT(CLOCK  : IN STD_LOGIC;
      RESET     : IN STD_LOGIC;
      LOAD  : IN STD_LOGIC;
      MAG   : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      PHS   : IN STD_LOGIC_VECTOR(15 DOWNTO 0);

      I         : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      Q         : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      P     : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      A     : OUT STD_LOGIC_VECTOR(17 DOWNTO 0)
      );

END COMPONENT;

COMPONENT MP2IQ_REG IS

PORT(CLOCK  : IN STD_LOGIC;
      RESET : IN STD_LOGIC;
      LOAD  : IN STD_LOGIC;
      I     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      Q     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      P     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      A     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      A_IN  : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      M     : IN INTEGER RANGE 0 TO 31;

      I_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      Q_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      P_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      A_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0)
      );

END COMPONENT;

COMPONENT IQ2MP_INIT IS

PORT(CLOCK  : IN STD_LOGIC;
      RESET     : IN STD_LOGIC;
      LOAD  : IN STD_LOGIC;
      I     : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      Q     : IN STD_LOGIC_VECTOR(15 DOWNTO 0);

      MAGI  : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      MAGQ  : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      A     : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      SIGN  : OUT STD_LOGIC
      );

END COMPONENT;

COMPONENT IQ2MP_REG IS

PORT(CLOCK  : IN STD_LOGIC;
      RESET : IN STD_LOGIC;
      LOAD  : IN STD_LOGIC;
      I     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      Q     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      A     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
--    A_IN  : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
--    M     : IN INTEGER RANGE 0 TO 31;

      I_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      Q_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      A_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      SIGN  : OUT STD_LOGIC
      );

END COMPONENT;

COMPONENT CORDIC_SHIFT IS
PORT(DATA_IN    : IN REG18_ARRAY;
     DATA_OUT   : OUT REG18_ARRAY
     );
END COMPONENT;

COMPONENT CORDIC_SHIFT_M IS
PORT(DATA_IN    : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      M         : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      DATA_OUT  : OUT STD_LOGIC_VECTOR(17 DOWNTO 0)
     );
END COMPONENT;

COMPONENT SQUARE_ROOT IS

    PORT(CLOCK      : IN STD_LOGIC;
         RESET      : IN STD_LOGIC;
         GO         : IN STD_LOGIC;
         DATA_IN        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
         DATA_OUT   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
         DONE_OUT   : OUT STD_LOGIC
         );

END COMPONENT;


COMPONENT IIR9_SIMPLE IS
    PORT(CLOCK  : IN STD_LOGIC;
         RESET  : IN STD_LOGIC;
         LOAD   : IN STD_LOGIC;
         I      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
         O      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
         );
END COMPONENT;

COMPONENT IIR924_SIMPLE IS
    PORT(CLOCK  : IN STD_LOGIC;
         RESET  : IN STD_LOGIC;
         LOAD   : IN STD_LOGIC;
         I      : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         O      : OUT STD_LOGIC_VECTOR(23 DOWNTO 0)
         );
END COMPONENT;
COMPONENT PT100_COMPARE IS
    PORT(CLOCK : IN STD_LOGIC;
         RESET : IN STD_LOGIC;
         TMP_DATA0 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA1 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA2 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA3 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA4 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA5 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA6 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA7 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT0 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT1 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT2 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT3 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT4 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT5 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT6 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT7 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_DISABLE : IN STD_LOGIC; -- Low-temperature check chicken-bit
         TMP_LO_LIMIT0 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT1 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT2 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT3 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT4 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT5 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT6 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT7 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         MASK_TMP_FAULT : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         FLT_CLR : IN STD_LOGIC;
         TMP_FAULT_CLEAR : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         LD_DATA : IN STD_LOGIC;
         TMP_FAULT : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         TMP_STATUS : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
         );
END COMPONENT;

COMPONENT dig_in_out is
port(clock      : in std_logic;
    reset       : in std_logic;

    dig_in1     : in std_logic;
    dig_in2     : in std_logic;
    dig_in3     : in std_logic;

    dig_out_epics   : in std_logic_vector(6 downto 0);

    dig_in_stat : out std_logic_vector(2 downto 0);
    dig_out1    : out std_logic;
    dig_out2    : out std_logic;
    dig_out3    : out std_logic;
    dig_out4    : out std_logic;
    dig_out5    : out std_logic;
    dig_out6    : out std_logic;
    dig_out7    : out std_logic
    );
END COMPONENT;

COMPONENT VAC_FAULT_STATUS IS
PORT(CLOCK : IN STD_LOGIC;
     RESET : IN STD_LOGIC;
     VAC_IN0 : IN STD_LOGIC;
     VAC_IN1 : IN STD_LOGIC;
     MASK_VAC : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
     FLT_CLR : IN STD_LOGIC;
     VAC_CLR : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
     VAC_FLT : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
     VAC_STATUS : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
     );
END COMPONENT;


COMPONENT CAV_FAULT_GEN IS
PORT(CLOCK : IN STD_LOGIC;
     RESET : IN STD_LOGIC;

     STP_TMP_FLT : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
     STP_TMP_MASK : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
     CPL_TMP_FLT : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     CPL_TMP_MASK : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     VAC_FLT : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
     VAC_FLT_MASK : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
     HE_FLT : IN STD_LOGIC;
     HE_FLT_MASK : IN STD_LOGIC;
     CAV_FLT_OUT : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
     );
END COMPONENT;

COMPONENT FLT_CLR_GEN IS
PORT(CLOCK : IN STD_LOGIC;
     RESET : IN STD_LOGIC;
     FLT_CLR_IN : IN STD_LOGIC;

     FLT_CLR_OUT : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
     );
END COMPONENT;

COMPONENT FC_FSD_OUT IS
PORT(CLOCK : IN STD_LOGIC;
     RESET : IN STD_LOGIC;

     CAV_FLT_IN : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
     FLT_CLR : IN STD_LOGIC;
     FC_FSD_MASK : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
     FC_FSD_CLR : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
     FC_FSD_STATUS : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
     );
END COMPONENT;

END COMPONENTS;

---------------------- N bit GENERIC regISter----------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;

ENTITY REGNE IS
        GENERIC(N : INTEGER := 16);
        PORT(CLOCK  : IN STD_LOGIC;
              RESET     : IN STD_LOGIC;
              CLEAR  : IN STD_LOGIC;
              EN        : IN STD_LOGIC;
              INPUT : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
              OUTPUT : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
             );
END ENTITY REGNE;

ARCHITECTURE REGNE of REGNE IS
BEGIN

    PROCESS(CLOCK,RESET)
    BEGIN
        IF(RESET = '0') THEN
            OUTPUT <= (OTHERS => '0');
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF(CLEAR = '0') THEN
                OUTPUT <= (OTHERS => '0');
            ELSIF(EN = '1') THEN
                OUTPUT <= INPUT;
            END IF;
        END IF;
    END PROCESS;
END ARCHITECTURE REGNE;


-----------------END of N bit gENeric regISter----------------


----------------------- N bit gENeric COUNTer----------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY COUNTER IS
        GENERIC(N : INTEGER := 5);
        PORT(CLOCK      : IN STD_LOGIC;
             RESET      : IN STD_LOGIC;
             CLEAR          : IN STD_LOGIC;
             ENABLE     : IN STD_LOGIC;
             COUNT      : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
            );
END ENTITY COUNTER;

ARCHITECTURE COUNTER of COUNTER IS
   SIGNAL COUNT_L : STD_LOGIC_VECTOR(N-1 DOWNTO 0) := (OTHERS => '0');
BEGIN
    PROCESS(CLOCK,RESET)
    BEGIN
        IF(RESET = '0') THEN
            COUNT_L <= (OTHERS => '0');
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF(CLEAR = '0') THEN
                COUNT_L <= (OTHERS => '0');
            ELSIF(ENABLE = '1') THEN
                COUNT_L <= COUNT_L + 1;
            END IF;
        END IF;
    END PROCESS;
    COUNT <= COUNT_L;
END ARCHITECTURE COUNTER;


---------------------END of N bit gENeric COUNTer----------------

--------FLIP with RESET --------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY FLIP_FLOP IS
    PORT(CLOCK  : IN STD_LOGIC;
          RESET     : IN STD_LOGIC;
          CLEAR  : IN STD_LOGIC;
          EN        : IN STD_LOGIC;
          INP   : IN STD_LOGIC;
          OUP   : OUT STD_LOGIC
         );
END ENTITY FLIP_FLOP;

ARCHITECTURE FLIP_FLOP of FLIP_FLOP IS
BEGIN

    PROCESS(CLOCK,RESET)
    BEGIN
        IF(RESET = '0') THEN
                OUP <= '0';
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF(CLEAR = '0') THEN
                OUP <= '0';
            ELSIF (EN = '1') THEN
                OUP <= INP;
            END IF;
        END IF;
    END PROCESS;
END ARCHITECTURE FLIP_FLOP;

--------END of FLIP FLOP with RESET --------------------

--------latch with set and reset --------------------

library ieee;
use ieee.std_logic_1164.all;

entity latch_n is
    port(clock : in std_logic;
         reset : in std_logic;
         clear  : in std_logic;
         en : in std_logic;
         inp : in std_logic;
         oup : out std_logic
        );
end entity latch_n;

architecture latch_n of latch_n is
begin

    process(clock,reset)
    begin
        if(reset = '0') then
                oup <= '0';
        elsif(clock = '1' and clock'event) then
            if(clear = '0') then
                oup <= '0';
            elsif (en = '1') then
                oup <= inp;
            end if;
        end if;
    end process;
end architecture latch_n;

--------end of latch with set and reset --------------------

--------shift register for LM74 temperature sensor----------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY SHIFT_LEFT_REG_AD7367 IS
    GENERIC(N : INTEGER := 16);
    PORT(CLOCK : IN STD_LOGIC;
         RESET : IN STD_LOGIC;
         EN : IN STD_LOGIC;
         INP : IN STD_LOGIC;
         OUTPUT : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
         );
END ENTITY SHIFT_LEFT_REG_AD7367;

ARCHITECTURE BEHAVIOR OF SHIFT_LEFT_REG_AD7367 IS

SIGNAL TEMP_OUT : STD_LOGIC_VECTOR(N-1 DOWNTO 0);

BEGIN


    PROCESS(CLOCK, RESET)
    BEGIN
        IF(RESET = '0') THEN
            TEMP_OUT <= (OTHERS => '0');
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF(EN = '1') THEN
                TEMP_OUT <= TEMP_OUT(N-2 DOWNTO 0) & INP;
            END IF;
        END IF;
    END PROCESS;

    OUTPUT  <= TEMP_OUT;

END ARCHITECTURE BEHAVIOR;



------SHIFT LEFT REGISTER ---------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY SHIFT_LEFT_REG IS
    GENERIC(N : INTEGER := 16);
    PORT(CLOCK  : IN STD_LOGIC;
          RESET     : IN STD_LOGIC;
          EN        : IN STD_LOGIC;
          LOAD  : IN STD_LOGIC;
          INP   : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          OUTPUT : OUT STD_LOGIC
          );
END ENTITY SHIFT_LEFT_REG;

ARCHITECTURE BEHAVIOR OF SHIFT_LEFT_REG IS

SIGNAL SHIFT_INPUT      : STD_LOGIC_VECTOR(N-1 DOWNTO 0);

BEGIN


    PROCESS(CLOCK, RESET)
    BEGIN
        IF(RESET = '0') THEN
            SHIFT_INPUT <= (OTHERS => '0');
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF(LOAD = '1') THEN
                SHIFT_INPUT <= INP;
            ELSIF(EN = '1') THEN
                SHIFT_INPUT <= SHIFT_INPUT(N-2 DOWNTO 0) & '0';
            END IF;
        END IF;
    END PROCESS;

    OUTPUT <= SHIFT_INPUT(N-1);

END ARCHITECTURE BEHAVIOR;

-------------------------------------------------------
------SHIFT LEFT REGISTER ---------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY SHIFT_LEFT_REG_CRC IS
    GENERIC(N : INTEGER := 16);
    PORT(CLOCK  : IN STD_LOGIC;
          RESET     : IN STD_LOGIC;
          EN        : IN STD_LOGIC;
          LOAD  : IN STD_LOGIC;
          CLEAR : IN STD_LOGIC;
          INP   : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          OUTPUT : OUT STD_LOGIC
          );
END ENTITY SHIFT_LEFT_REG_CRC;

ARCHITECTURE BEHAVIOR OF SHIFT_LEFT_REG_CRC IS

SIGNAL SHIFT_INPUT      : STD_LOGIC_VECTOR(N-1 DOWNTO 0);

BEGIN


    PROCESS(CLOCK, RESET)
    BEGIN
        IF(RESET = '0') THEN
            SHIFT_INPUT <= (OTHERS => '0');
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF (CLEAR = '0') THEN
                SHIFT_INPUT <= (OTHERS => '0');
            ELSIF(LOAD = '1') THEN
                SHIFT_INPUT <= INP;
            ELSIF(EN = '1') THEN
                SHIFT_INPUT <= SHIFT_INPUT(N-2 DOWNTO 0) & '0';
            END IF;
        END IF;
    END PROCESS;

    OUTPUT <= SHIFT_INPUT(N-1);

END ARCHITECTURE BEHAVIOR;

-------------------------------------------------------


------SHIFT LEFT REGISTER WITH INPUT BIT---------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY SHIFT_LEFT_BIT IS
    GENERIC(N : INTEGER := 16);
    PORT(CLOCK      : IN STD_LOGIC;
          RESET         : IN STD_LOGIC;
          EN            : IN STD_LOGIC;
          LOAD      : IN STD_LOGIC;
          INP       : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          SHIFT_BIT : IN STD_LOGIC;
          OUTPUT    : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
          );
END ENTITY SHIFT_LEFT_BIT;

ARCHITECTURE BEHAVIOR OF SHIFT_LEFT_BIT IS

SIGNAL SHIFT_INPUT      : STD_LOGIC_VECTOR(N-1 DOWNTO 0);

BEGIN


    PROCESS(CLOCK, RESET)
    BEGIN
        IF(RESET = '0') THEN
            SHIFT_INPUT <= (OTHERS => '0');
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF(LOAD = '1') THEN
                SHIFT_INPUT <= INP;
            ELSIF(EN = '1') THEN
                SHIFT_INPUT <= SHIFT_INPUT(N-2 DOWNTO 0) & SHIFT_BIT;
            END IF;
        END IF;
    END PROCESS;

    OUTPUT <= SHIFT_INPUT;

END ARCHITECTURE BEHAVIOR;

-------------------------------------------------------


------N-BIT GENERIC ADDER-----------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY ADDER IS
        GENERIC(N : INTEGER := 16);
        PORT(AIN        : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
              BIN       : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
              SUM       : OUT STD_LOGIC_VECTOR(N DOWNTO 0)
              );
END ENTITY ADDER;

ARCHITECTURE BEHAVIOR OF ADDER IS

BEGIN

SUM <= (AIN(N-1)&AIN) + (BIN(N-1)&BIN);

END ARCHITECTURE BEHAVIOR;

--------END OF N-BIT GENERIC ADDER----------------

--------DIVISION BY EVEN NUMBER M--------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY SHIFT_BY_M IS
    GENERIC(N : INTEGER := 18);
    PORT(D_IN   : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          M         : IN     INTEGER RANGE 0 TO 31;
          D_OUT : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
          );
END ENTITY SHIFT_BY_M;

ARCHITECTURE BEHAVIOR OF SHIFT_BY_M IS


SIGNAL D_INT : STD_LOGIC_VECTOR(N-1 DOWNTO 0);
--SIGNAL ZERO : STD_LOGIC_VECTOR(N-1 DOWNTO 0);

BEGIN



PROCESS(M, D_IN)
BEGIN
        IF M = 0 THEN
            D_INT <= D_IN;
        ELSIF (M = N OR M > N) THEN
            D_INT <= (OTHERS => '0');
        ELSIF M = 1 THEN
            D_INT(N-1)<= D_IN(N-1);
            D_INT(N-2 DOWNTO 0)     <= D_IN(N-1 DOWNTO 1);
        ELSE
            D_INT(N-1 DOWNTO N-M)   <= (OTHERS => D_IN(N-1));
            D_INT(N-M-1 DOWNTO 0)   <= D_IN(N-1 DOWNTO M);
        END IF;
END PROCESS;

D_OUT       <= D_INT;

END ARCHITECTURE BEHAVIOR;

------------------   MP2IQ REGISTER---------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.COMPONENTS.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY MP2IQ_REG IS

PORT(CLOCK  : IN STD_LOGIC;
      RESET : IN STD_LOGIC;
      LOAD  : IN STD_LOGIC;
      I     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      Q     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      P     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      A     : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      A_IN  : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      M     : IN INTEGER RANGE 0 TO 31;

      I_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      Q_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      P_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      A_OUT : OUT STD_LOGIC_VECTOR(17 DOWNTO 0)
      );

END ENTITY MP2IQ_REG;

ARCHITECTURE BEHAVIOR OF MP2IQ_REG IS

SIGNAL I_DIV            : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL Q_DIV            : STD_LOGIC_VECTOR(17 DOWNTO 0);

SIGNAL I_INT            : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL Q_INT            : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL ANG_INT          : STD_LOGIC_VECTOR(17 DOWNTO 0);

SIGNAL SIGN             : STD_LOGIC;

SIGNAL SIGN_INT     : STD_LOGIC_VECTOR(17 DOWNTO 0);

BEGIN


SIGN_INT    <= P - A;
SIGN            <= SIGN_INT(17);


I_SHIFT: SHIFT_BY_M ----DIVISION BY 2^M
    GENERIC MAP(N => 18)

    PORT MAP(D_IN   =>  I,
                M       => M,
                D_OUT   => I_DIV
                );

Q_SHIFT: SHIFT_BY_M ----DIVISION BY 2^M
    GENERIC MAP(N => 18)

    PORT MAP(D_IN   =>  Q,
                M       => M,
                D_OUT   => Q_DIV
                );


I_INT <= I - Q_DIV WHEN SIGN = '0' ELSE I + Q_DIV;

I_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => I_INT,
                    OUTPUT  => I_OUT
                    );

Q_INT <= Q - I_DIV WHEN SIGN = '1' ELSE Q + I_DIV;
Q_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => Q_INT,
                    OUTPUT  => Q_OUT
                    );

ANG_INT <= A - A_IN WHEN SIGN = '1' ELSE A + A_IN;
ANG_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => ANG_INT,
                    OUTPUT  => A_OUT
                    );

PHS_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => P,
                    OUTPUT  => P_OUT
                    );





END ARCHITECTURE BEHAVIOR;

---------------END OF MP2IQ REGISTER----------------

---------MP2IQINIT REGISTER------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY MP2IQ_INIT IS

PORT(CLOCK  : IN STD_LOGIC;
      RESET     : IN STD_LOGIC;
      LOAD  : IN STD_LOGIC;
      MAG   : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      PHS   : IN STD_LOGIC_VECTOR(15 DOWNTO 0);

      I         : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      Q         : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      P     : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      A     : OUT STD_LOGIC_VECTOR(17 DOWNTO 0)
      );

END ENTITY MP2IQ_INIT;

ARCHITECTURE BEHAVIOR OF MP2IQ_INIT IS

SIGNAL MAG_INT  : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL I_INT    : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL Q_INT    : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL ANG_INT  : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL PHS_INT  : STD_LOGIC_VECTOR(17 DOWNTO 0);

BEGIN

MAG_INT(17 DOWNTO 16)   <= "00";
MAG_INT(15 DOWNTO 0)    <= MAG;

PHS_INT(17 DOWNTO 2)    <= PHS ;
PHS_INT(1 DOWNTO 0)     <= "00";

--PHS_INT(17 DOWNTO 16) <= (OTHERS => PHS(15)) ;
--PHS_INT(15 DOWNTO 0)      <= PHS;

I_INT <= (OTHERS => '0');

Q_INT <= MAG_INT WHEN PHS(15) = '0' ELSE (NOT MAG_INT + '1') ;


ANG_INT <= ("01" & X"0000") WHEN PHS(15) = '0' ELSE ("11" & X"0000");--65536 POS 90 DEG, 196608 NEG 90 DEG

--ANG_INT <= ("00" & X"4000") WHEN PHS(15) = '0' ELSE ("11" & X"C000");--65536 POS 90 DEG, 196608 NEG 90 DEG


I_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => I_INT,
                    OUTPUT  => I
                    );

Q_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => Q_INT,
                    OUTPUT  => Q
                    );

ANG_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => ANG_INT,
                    OUTPUT  => A
                    );

PHS_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => PHS_INT,
                    OUTPUT  => P
                    );

END ARCHITECTURE BEHAVIOR;

--------END OF MP2IQ INIT REGISTER-----------

---------IQ2MPINIT REGISTER------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY IQ2MP_INIT IS

PORT(CLOCK  : IN STD_LOGIC;
      RESET     : IN STD_LOGIC;
      LOAD  : IN STD_LOGIC;
      I     : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      Q     : IN STD_LOGIC_VECTOR(15 DOWNTO 0);

      MAGI  : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      MAGQ  : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      A     : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      SIGN  : OUT STD_LOGIC
      );

END ENTITY IQ2MP_INIT;

ARCHITECTURE BEHAVIOR OF IQ2MP_INIT IS

SIGNAL MAGI_INT : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL MAGQ_OUT_INT : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL MAGQ_INT : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL I_INT    : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL Q_INT    : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL ANG_INT  : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL PHS_INT  : STD_LOGIC_VECTOR(17 DOWNTO 0);

BEGIN

I_INT(17 DOWNTO 16)     <= (OTHERS => I(15));
I_INT(15 DOWNTO 0)      <= I;

Q_INT(17 DOWNTO 16)     <= (OTHERS => Q(15)) ;
Q_INT(15 DOWNTO 0)      <= Q;

--PHS_INT(17 DOWNTO 16) <= (OTHERS => PHS(15)) ;
--PHS_INT(15 DOWNTO 0)      <= PHS;

ANG_INT                     <= ("01" & X"0000") WHEN Q(15) = '1' ELSE ("11" & X"0000");--65536 POS 90 DEG, 196608 NEG 90 DEG

--ANG_INT <= ("00" & X"4000") WHEN PHS(15) = '0' ELSE ("11" & X"C000");--65536 POS 90 DEG, 196608 NEG 90 DEG

MAGI_INT                    <= Q_INT WHEN Q(15) = '0' ELSE ((NOT Q_INT) + '1');
MAGQ_INT                    <= I_INT WHEN Q(15) = '1' ELSE ((NOT I_INT) + '1');

SIGN                        <= MAGQ_OUT_INT(17);


MAGI_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => MAGI_INT,
                    OUTPUT  => MAGI
                    );

MAGQ_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => MAGQ_INT,
                    OUTPUT      => MAGQ_OUT_INT
                    );

MAGQ        <= MAGQ_OUT_INT;

ANG_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => ANG_INT,
                    OUTPUT  => A
                    );


END ARCHITECTURE BEHAVIOR;

--------END OF IQ2MP INIT REGISTER-----------

---------------IQ2MP REGISTER----------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.COMPONENTS.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY IQ2MP_REG IS

PORT(CLOCK      : IN STD_LOGIC;
      RESET     : IN STD_LOGIC;
      LOAD      : IN STD_LOGIC;
      I         : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      Q         : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      A         : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
--    A_IN      : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
--    M         : IN INTEGER RANGE 0 TO 31;

      I_OUT     : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      Q_OUT     : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      A_OUT     : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
      SIGN      : OUT STD_LOGIC
      );

END ENTITY IQ2MP_REG;

ARCHITECTURE BEHAVIOR OF IQ2MP_REG IS

SIGNAL SIGN_INT     : STD_LOGIC;

SIGNAL I_INT        : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL Q_INT        : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL ANG_INT      : STD_LOGIC_VECTOR(17 DOWNTO 0);

SIGNAL Q_OUT_INT    : STD_LOGIC_VECTOR(17 DOWNTO 0);


SIGNAL I_DIV        : STD_LOGIC_VECTOR(17 DOWNTO 0);
SIGNAL Q_DIV        : STD_LOGIC_VECTOR(17 DOWNTO 0);


BEGIN

SIGN_INT        <= Q_OUT_INT(17);
SIGN            <= SIGN_INT;

--I_SHIFT: SHIFT_BY_M ----DIVISION BY 2^M
--  GENERIC MAP(N => 18)
--
--  PORT MAP(D_IN   =>  I,
--              M       => M,
--              D_OUT   => I_DIV
--              );
--
--Q_SHIFT: SHIFT_BY_M ----DIVISION BY 2^M
--  GENERIC MAP(N => 18)
--
--  PORT MAP(D_IN   =>  Q,
--              M       => M,
--              D_OUT   => Q_DIV
--              );
--
--I_INT <= I - Q_DIV WHEN SIGN = '1' ELSE I + Q_DIV;

MAGI_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => I,
                    OUTPUT  => I_OUT
                    );

--Q_INT <= Q - I_DIV WHEN SIGN = '0' ELSE Q + I_DIV;

MAGQ_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => Q,
                    OUTPUT      => Q_OUT_INT
                    );

Q_OUT <= Q_OUT_INT;

--ANG_INT <= A - A_IN WHEN Q(17) = '0' ELSE A + A_IN;
ANG_REG: REGNE
        GENERIC MAP(N => 18)
        PORT MAP(CLOCK      => CLOCK,
                    RESET       => RESET,
                    CLEAR       => '1',
                    EN          => LOAD,
                    INPUT       => A,
                    OUTPUT  => A_OUT
                    );


END ARCHITECTURE BEHAVIOR;

---------END OF IQ2MP REGISTER-------------------
----------------shift register as a block----------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY CORDIC_SHIFT IS
PORT(DATA_IN    : IN REG18_ARRAY;
     DATA_OUT   : OUT REG18_ARRAY
     );
END ENTITY CORDIC_SHIFT;

ARCHITECTURE BEHAVIOR OF CORDIC_SHIFT IS
BEGIN

--DATA_OUT(0)                       <= DATA_IN(0);

DATA_OUT(0)(17)                 <= DATA_IN(0)(17);
DATA_OUT(0)(16 DOWNTO 0)        <= DATA_IN(0)(17 DOWNTO 1);

DATA_OUT(1)(17 DOWNTO 16)       <= (OTHERS => DATA_IN(1)(17));
DATA_OUT(1)(15 DOWNTO 0)        <= DATA_IN(1)(17 DOWNTO 2);

DATA_OUT(2)(17 DOWNTO 15)       <= (OTHERS => DATA_IN(2)(17));
DATA_OUT(2)(14 DOWNTO 0)        <= DATA_IN(2)(17 DOWNTO 3);

DATA_OUT(3)(17 DOWNTO 14)       <= (OTHERS => DATA_IN(3)(17));
DATA_OUT(3)(13 DOWNTO 0)        <= DATA_IN(3)(17 DOWNTO 4);

DATA_OUT(4)(17 DOWNTO 13)       <= (OTHERS => DATA_IN(4)(17));
DATA_OUT(4)(12 DOWNTO 0)        <= DATA_IN(4)(17 DOWNTO 5);

DATA_OUT(5)(17 DOWNTO 12)       <= (OTHERS => DATA_IN(5)(17));
DATA_OUT(5)(11 DOWNTO 0)        <= DATA_IN(5)(17 DOWNTO 6);

DATA_OUT(6)(17 DOWNTO 11)       <= (OTHERS => DATA_IN(6)(17));
DATA_OUT(6)(10 DOWNTO 0)        <= DATA_IN(6)(17 DOWNTO 7);

DATA_OUT(7)(17 DOWNTO 10)       <= (OTHERS => DATA_IN(7)(17));
DATA_OUT(7)(9 DOWNTO 0)         <= DATA_IN(7)(17 DOWNTO 8);

DATA_OUT(8)(17 DOWNTO 9)        <= (OTHERS => DATA_IN(8)(17));
DATA_OUT(8)(8 DOWNTO 0)         <= DATA_IN(8)(17 DOWNTO 9);

DATA_OUT(9)(17 DOWNTO 8)        <= (OTHERS => DATA_IN(9)(17));
DATA_OUT(9)(7 DOWNTO 0)         <= DATA_IN(9)(17 DOWNTO 10);

DATA_OUT(10)(17 DOWNTO 7)       <= (OTHERS => DATA_IN(10)(17));
DATA_OUT(10)(6 DOWNTO 0)        <= DATA_IN(10)(17 DOWNTO 11);

DATA_OUT(11)(17 DOWNTO 6)       <= (OTHERS => DATA_IN(11)(17));
DATA_OUT(11)(5 DOWNTO 0)        <= DATA_IN(11)(17 DOWNTO 12);

DATA_OUT(12)(17 DOWNTO 5)       <= (OTHERS => DATA_IN(12)(17));
DATA_OUT(12)(4 DOWNTO 0)        <= DATA_IN(12)(17 DOWNTO 13);

DATA_OUT(13)(17 DOWNTO 4)       <= (OTHERS => DATA_IN(13)(17));
DATA_OUT(13)(3 DOWNTO 0)        <= DATA_IN(13)(17 DOWNTO 14);

DATA_OUT(14)(17 DOWNTO 3)       <= (OTHERS => DATA_IN(14)(17));
DATA_OUT(14)(2 DOWNTO 0)        <= DATA_IN(14)(17 DOWNTO 15);

DATA_OUT(15)                        <= (OTHERS => '0');

END ARCHITECTURE;

------------------------cordic shift for iq2mp or mp2iq small--------------------

----------------shift register as a block----------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY CORDIC_SHIFT_M IS
PORT(DATA_IN    : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
      M         : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      DATA_OUT  : OUT STD_LOGIC_VECTOR(17 DOWNTO 0)
     );
END ENTITY CORDIC_SHIFT_M;

ARCHITECTURE BEHAVIOR OF CORDIC_SHIFT_M IS
BEGIN

PROCESS(DATA_IN, M)
BEGIN
    CASE M IS

        WHEN "0000"     => DATA_OUT <= DATA_IN;

        WHEN "0001"     => DATA_OUT(17)                     <= DATA_IN(17);
                                DATA_OUT(16 DOWNTO 0)       <= DATA_IN(17 DOWNTO 1);

        WHEN "0010"     => DATA_OUT(17 DOWNTO 16)       <= (OTHERS => DATA_IN(17));
                                DATA_OUT(15 DOWNTO 0)       <= DATA_IN(17 DOWNTO 2);

        WHEN "0011"     => DATA_OUT(17 DOWNTO 15)       <= (OTHERS => DATA_IN(17));
                                DATA_OUT(14 DOWNTO 0)       <= DATA_IN(17 DOWNTO 3);

        WHEN "0100"     => DATA_OUT(17 DOWNTO 14)       <= (OTHERS => DATA_IN(17));
                                DATA_OUT(13 DOWNTO 0)       <= DATA_IN(17 DOWNTO 4);

        WHEN "0101"     => DATA_OUT(17 DOWNTO 13)       <= (OTHERS => DATA_IN(17));
                                DATA_OUT(12 DOWNTO 0)       <= DATA_IN(17 DOWNTO 5);

        WHEN "0110"     => DATA_OUT(17 DOWNTO 12)       <= (OTHERS => DATA_IN(17));
                                DATA_OUT(11 DOWNTO 0)       <= DATA_IN(17 DOWNTO 6);

        WHEN "0111"     => DATA_OUT(17 DOWNTO 11)       <= (OTHERS => DATA_IN(17));
                                DATA_OUT(10 DOWNTO 0)       <= DATA_IN(17 DOWNTO 7);

        WHEN "1000"     => DATA_OUT(17 DOWNTO 10)       <= (OTHERS => DATA_IN(17));
                                DATA_OUT(9 DOWNTO 0)            <= DATA_IN(17 DOWNTO 8);

        WHEN "1001"     => DATA_OUT(17 DOWNTO 9)        <= (OTHERS => DATA_IN(17));
                                DATA_OUT(8 DOWNTO 0)            <= DATA_IN(17 DOWNTO 9);

        WHEN "1010"     => DATA_OUT(17 DOWNTO 8)        <= (OTHERS => DATA_IN(17));
                                DATA_OUT(7 DOWNTO 0)            <= DATA_IN(17 DOWNTO 10);

        WHEN "1011"     => DATA_OUT(17 DOWNTO 7)        <= (OTHERS => DATA_IN(17));
                                DATA_OUT(6 DOWNTO 0)            <= DATA_IN(17 DOWNTO 11);

        WHEN "1100"     => DATA_OUT(17 DOWNTO 6)        <= (OTHERS => DATA_IN(17));
                                DATA_OUT(5 DOWNTO 0)            <= DATA_IN(17 DOWNTO 12);

        WHEN "1101"     => DATA_OUT(17 DOWNTO 5)        <= (OTHERS => DATA_IN(17));
                                DATA_OUT(4 DOWNTO 0)            <= DATA_IN(17 DOWNTO 13);

        WHEN "1110"     => DATA_OUT(17 DOWNTO 4)        <= (OTHERS => DATA_IN(17));
                                DATA_OUT(3 DOWNTO 0)            <= DATA_IN(17 DOWNTO 14);

        WHEN "1111"     => DATA_OUT(17 DOWNTO 3)        <= (OTHERS => DATA_IN(17));
                                DATA_OUT(2 DOWNTO 0)            <= DATA_IN(17 DOWNTO 15);
        WHEN others     => DATA_OUT <= (others => '-');

    END CASE;

END PROCESS;

END ARCHITECTURE;

------SHIFT LEFT REGISTER for square root---------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY SHIFT_SQUARE_ROOT IS
    GENERIC(N : INTEGER := 64);
    PORT(CLOCK      : IN STD_LOGIC;
          RESET     : IN STD_LOGIC;
          CLEAR     : IN STD_LOGIC;
          EN        : IN STD_LOGIC;
          LOAD      : IN STD_LOGIC;
          INP       : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          SHIFT_IN  : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
          OUTPUT    : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          SHIFT_BIT : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)

          );
END ENTITY;

ARCHITECTURE BEHAVIOR OF SHIFT_SQUARE_ROOT IS

SIGNAL SHIFT_INPUT      : STD_LOGIC_VECTOR(N-1 DOWNTO 0);


BEGIN



    PROCESS(CLOCK, RESET)
    BEGIN
        IF(RESET = '0') THEN
            SHIFT_INPUT <= (OTHERS => '0');
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF (CLEAR = '0') THEN
                SHIFT_INPUT <= (OTHERS => '0');
            ELSIF(LOAD = '1') THEN
                SHIFT_INPUT <= INP;
            ELSIF(EN = '1') THEN
                SHIFT_INPUT <= SHIFT_INPUT(N-3 DOWNTO 0) & SHIFT_IN;
            END IF;
        END IF;
    END PROCESS;

    OUTPUT      <= SHIFT_INPUT;
    SHIFT_BIT   <= SHIFT_INPUT(N-1 DOWNTO N-2);


END ARCHITECTURE BEHAVIOR;

-------------------------------------------------------

--------------------------SQUARE ROOT OF A 32 BIT NUMBER----------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE WORK.COMPONENTS.ALL;


ENTITY SQUARE_ROOT IS

    PORT(CLOCK      : IN STD_LOGIC;
         RESET      : IN STD_LOGIC;
         GO         : IN STD_LOGIC;
         DATA_IN    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
         DATA_OUT   : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
         DONE_OUT   : OUT STD_LOGIC
         );

END ENTITY SQUARE_ROOT;

ARCHITECTURE BEHAVIOR OF SQUARE_ROOT IS

SIGNAL RADICAND                 : STD_LOGIC_VECTOR(63 DOWNTO 0);
SIGNAL SHIFT_RADICAND_OUT       : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL SHIFT_RADICAND_INT       : STD_LOGIC_VECTOR(63 DOWNTO 0);
SIGNAL EN_SHIFT_RADICAND        : STD_LOGIC;
SIGNAL LD_RADICAND              : STD_LOGIC;
SIGNAL CLR_RADICAND             : STD_LOGIC;

SIGNAL EN_QUOTIENT              : STD_LOGIC;
SIGNAL LD_QUOTIENT              : STD_LOGIC;
SIGNAL QUOTIENT_BIT             : STD_LOGIC;
SIGNAL QUOTIENT_INT             : STD_LOGIC_VECTOR(31 DOWNTO 0);

SIGNAL SHIFT_SUBTRACT_A         : STD_LOGIC;
SIGNAL LD_SUBTRACT_A            : STD_LOGIC;
SIGNAL SUBTRACT_RESULT          : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL SUBTRACT_A               : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL SUBTRACT_A_SHIFT         : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL CLR_SUBTRACT_A           : STD_LOGIC;

SIGNAL SHIFT_SUBTRACT_B         : STD_LOGIC;
SIGNAL LD_SUBTRACT_B            : STD_LOGIC;
SIGNAL SUBTRACT_B               : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL SUBTRACT_B_SHIFT         : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL CLR_SUBTRACT_B           : STD_LOGIC;

SIGNAL EN_LOOP_COUNT            : STD_LOGIC;
SIGNAL LOOP_COUNT               : STD_LOGIC_VECTOR(4 DOWNTO 0);

SIGNAL EN_RESULT                : STD_LOGIC;

TYPE STATE_TYPE IS (INIT, LOAD_QUOTIENT_INT, LOAD_DATA, LOAD_SUB_REG, ENA_QUOTIENT, SHIFT_RADICAND, ENA_SUB_REG, DONE);
SIGNAL STATE                    : STATE_TYPE;

BEGIN

RADICAND        <= DATA_IN & X"00000000";

RADICAND_REG: SHIFT_SQUARE_ROOT
    GENERIC MAP(N => 64)
    PORT MAP(CLOCK      => CLOCK,
             RESET      => RESET,
             CLEAR      => CLR_RADICAND,
             EN         => EN_SHIFT_RADICAND,
             LOAD       => LD_RADICAND,
             INP        => RADICAND,
             SHIFT_IN   => "00",
             OUTPUT     => SHIFT_RADICAND_INT,
             SHIFT_BIT  => SHIFT_RADICAND_OUT
             );

QUOTIENT_REG: SHIFT_LEFT_BIT
    GENERIC MAP(N => 32)
    PORT MAP(CLOCK      => CLOCK,
             RESET      => RESET,
             EN         => EN_QUOTIENT,
             LOAD       => LD_QUOTIENT,
             INP        => X"00000000",
             SHIFT_BIT  => QUOTIENT_BIT,
             OUTPUT     => QUOTIENT_INT
             );

SQUARE_ROOT_LOOP_COUNTER: COUNTER
        GENERIC MAP(N => 5)
        PORT MAP(CLOCK      => CLOCK,
                 RESET      => RESET,
                 CLEAR      => '1',
                 ENABLE     => EN_LOOP_COUNT,
                 COUNT      => LOOP_COUNT
                 );

SUBTRACT_REGA: SHIFT_SQUARE_ROOT
    GENERIC MAP(N => 32)
    PORT MAP(CLOCK      => CLOCK,
             RESET      => RESET,
             CLEAR      => CLR_SUBTRACT_A,
             EN         => SHIFT_SUBTRACT_A,
             LOAD       => LD_SUBTRACT_A,
             SHIFT_IN   => SHIFT_RADICAND_INT(63 DOWNTO 62),
             INP        => SUBTRACT_RESULT,
             OUTPUT     => SUBTRACT_A,
             SHIFT_BIT  => SUBTRACT_A_SHIFT
             );

SUBTRACT_REGB: SHIFT_SQUARE_ROOT
    GENERIC MAP(N => 32)
    PORT MAP(CLOCK      => CLOCK,
             RESET      => RESET,
             CLEAR      => CLR_SUBTRACT_B,
             EN         => SHIFT_SUBTRACT_B,
             LOAD       => LD_SUBTRACT_B,
             SHIFT_IN   => "01",
             INP        => QUOTIENT_INT,
             OUTPUT     => SUBTRACT_B,
             SHIFT_BIT  => SUBTRACT_B_SHIFT
             );

SUBTRACT_RESULT     <= SUBTRACT_A - SUBTRACT_B WHEN QUOTIENT_BIT = '1' ELSE SUBTRACT_A;

QUOTIENT_BIT    <= '1' WHEN (SUBTRACT_A > SUBTRACT_B OR SUBTRACT_A = SUBTRACT_B) AND SUBTRACT_A /= X"00000000" ELSE '0';

RESULT_REG: REGNE
        GENERIC MAP(N => 32)
        PORT MAP(CLOCK  => CLOCK,
                 RESET  => RESET,
                 CLEAR  => '1',
                 EN     => EN_RESULT,
                 INPUT  => QUOTIENT_INT,
                 OUTPUT => DATA_OUT
                 );


    PROCESS(CLOCK, RESET)
    BEGIN
        IF RESET = '0' THEN
            STATE <= INIT;
        ELSIF CLOCK = '1' AND CLOCK'EVENT THEN

            CASE STATE IS

                WHEN INIT               => IF GO = '1' THEN STATE <= LOAD_QUOTIENT_INT;
                                            ELSE STATE <= INIT;
                                            END IF;

                WHEN LOAD_QUOTIENT_INT  => STATE <= LOAD_DATA;

                WHEN LOAD_DATA          => STATE <= LOAD_SUB_REG;

                WHEN LOAD_SUB_REG       => STATE <= ENA_QUOTIENT;

                WHEN ENA_QUOTIENT       => IF LOOP_COUNT /= "11111" THEN STATE <= SHIFT_RADICAND;
                                             ELSE STATE <= DONE;
                                            END IF;

                WHEN SHIFT_RADICAND     => STATE <= ENA_SUB_REG;

                WHEN ENA_SUB_REG        => STATE <= ENA_QUOTIENT;

                WHEN DONE               => STATE <= INIT;

                WHEN OTHERS             => STATE <= INIT;

            END CASE;
        END IF;
    END PROCESS;

LD_RADICAND         <= '1' WHEN STATE = LOAD_DATA ELSE '0';
LD_QUOTIENT         <= '1' WHEN STATE = LOAD_QUOTIENT_INT ELSE '0';

EN_SHIFT_RADICAND   <= '1' WHEN STATE = SHIFT_RADICAND ELSE '0';

LD_SUBTRACT_A       <= '1' WHEN STATE = LOAD_DATA OR STATE = SHIFT_RADICAND ELSE '0';
LD_SUBTRACT_B       <= '1' WHEN STATE = LOAD_DATA OR STATE = SHIFT_RADICAND ELSE '0';

SHIFT_SUBTRACT_A    <= '1' WHEN STATE = LOAD_SUB_REG OR STATE = ENA_SUB_REG ELSE '0';
SHIFT_SUBTRACT_B    <= '1' WHEN STATE = LOAD_SUB_REG OR STATE = ENA_SUB_REG ELSE '0';

EN_QUOTIENT         <= '1' WHEN STATE = ENA_QUOTIENT ELSE '0';

EN_RESULT           <= '1' WHEN STATE = DONE ELSE '0';
DONE_OUT            <= '1' WHEN STATE = DONE ELSE '0';

EN_LOOP_COUNT       <= '1' WHEN STATE = ENA_QUOTIENT ELSE '0';

CLR_RADICAND        <= '0' WHEN STATE = DONE ELSE '1';

CLR_SUBTRACT_A      <= '0' WHEN STATE = DONE ELSE '1';

CLR_SUBTRACT_B      <= '0' WHEN STATE = DONE ELSE '1';




END ARCHITECTURE BEHAVIOR;



------SHIFT RIGHT REGISTER FOR AD7328---------

LIBRARY IEEE;

USE IEEE.STD_LOGIC_1164.ALL;



ENTITY SHIFT_REG IS

    GENERIC(N : INTEGER := 16);
    PORT(CLOCK : IN STD_LOGIC;
         RESET : IN STD_LOGIC;
         EN : IN STD_LOGIC;
         LOAD : IN STD_LOGIC;
         INP : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
         OUTPUT : OUT STD_LOGIC
         );
     END ENTITY SHIFT_REG;
 ARCHITECTURE BEHAVIOR OF SHIFT_REG IS
 SIGNAL SHIFT_INPUT     : STD_LOGIC_VECTOR(N-1 DOWNTO 0);
 BEGIN
    PROCESS(CLOCK, RESET)
    BEGIN
        IF(RESET = '0') THEN
            SHIFT_INPUT <= (OTHERS => '0');
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF(LOAD = '1') THEN
                SHIFT_INPUT <= INP;
            ELSIF(EN = '1') THEN
                SHIFT_INPUT <= SHIFT_INPUT(N-2 DOWNTO 0) & '0';
            END IF;
        END IF;
    END PROCESS;
                                                                                                        OUTPUT <= SHIFT_INPUT(N-1);
END ARCHITECTURE BEHAVIOR;

------SHIFT LEFT REGISTER ---------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY SHIFT_LEFT_REG_ADS8353 IS
    GENERIC(N : INTEGER := 16);
    PORT(CLOCK  : IN STD_LOGIC;
          RESET     : IN STD_LOGIC;
          EN        : IN STD_LOGIC;
          LOAD  : IN STD_LOGIC;
          INP   : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
          OUTPUT : OUT STD_LOGIC
          );
END ENTITY SHIFT_LEFT_REG_ADS8353;

ARCHITECTURE BEHAVIOR OF SHIFT_LEFT_REG_ADS8353 IS

SIGNAL SHIFT_INPUT      : STD_LOGIC_VECTOR(N-1 DOWNTO 0);

BEGIN


    PROCESS(CLOCK, RESET)
    BEGIN
        IF(RESET = '0') THEN
            SHIFT_INPUT <= (OTHERS => '0');
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF(LOAD = '1') THEN
                SHIFT_INPUT <= INP;
            ELSIF(EN = '1') THEN
                SHIFT_INPUT <= SHIFT_INPUT(N-2 DOWNTO 0) & '0';
            END IF;
        END IF;
    END PROCESS;

    OUTPUT <= SHIFT_INPUT(N-1);

END ARCHITECTURE BEHAVIOR;

-------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY SHIFT_REG_CRC IS
        GENERIC (N :INTEGER := 16);
        PORT (CLOCK : IN STD_LOGIC;
              RESET : IN STD_LOGIC;
              CLR   : IN STD_LOGIC;
              EN    : IN STD_LOGIC;
              INP   : IN STD_LOGIC;
              OUTPUT : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0)
         );
END ENTITY SHIFT_REG_CRC;

ARCHITECTURE BEHAVIOR OF SHIFT_REG_CRC IS
SIGNAL TEMP_OUT : STD_LOGIC_VECTOR(N-1 DOWNTO 0);
BEGIN

    PROCESS(CLOCK, RESET)
    BEGIN
        IF(RESET = '0') THEN
            TEMP_OUT <= (OTHERS => '0');
        ELSIF(CLOCK = '1' AND CLOCK'EVENT) THEN
            IF (CLR = '0') THEN
                TEMP_OUT <= (OTHERS => '0');
            ELSIF (EN = '1') THEN
                TEMP_OUT <= TEMP_OUT(N-2 DOWNTO 0) & INP;
            END IF;
        END IF;
    END PROCESS;

    OUTPUT <= TEMP_OUT;
END ARCHITECTURE BEHAVIOR;


----------------------------------------IIR9 SIMPLE FILTER-------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY IIR9_SIMPLE IS
    PORT(CLOCK  : IN STD_LOGIC;
         RESET  : IN STD_LOGIC;
         LOAD   : IN STD_LOGIC;
         I      : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
         O      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
         );
END ENTITY IIR9_SIMPLE;
ARCHITECTURE BEHAVIOR OF IIR9_SIMPLE IS
SIGNAL I_EXT        : STD_LOGIC_VECTOR(44 DOWNTO 0);
SIGNAL REG_SHIFT    : STD_LOGIC_VECTOR(44 DOWNTO 0);
SIGNAL REG      : STD_LOGIC_VECTOR(44 DOWNTO 0);
SIGNAL ONE      : STD_LOGIC;
SIGNAL REG_IN       : STD_LOGIC_VECTOR(44 DOWNTO 0);
SIGNAL REG_OUT      : STD_LOGIC_VECTOR(44 DOWNTO 0);
SIGNAL O_INT        : STD_LOGIC_VECTOR(31 DOWNTO 0);
BEGIN
I_EXT(44 DOWNTO 32) <= (OTHERS => I(31));
I_EXT(31 DOWNTO 0) <= I;
FILTER_REG: REGNE
    GENERIC MAP(N => 45)
    PORT MAP(CLOCK  => CLOCK,
        RESET   => RESET,
        CLEAR   => '1',
        EN  => LOAD,
        INPUT   => REG_IN,
        OUTPUT  => REG_OUT
        );

REG_IN  <= REG_OUT - REG_SHIFT + I_EXT;

REG_SHIFT(44 DOWNTO 36) <= (OTHERS => REG_OUT(44));
REG_SHIFT(35 DOWNTO 0) <= REG_OUT(44 DOWNTO 9);
O_INT <= X"7FFFFFFF" WHEN REG_OUT(44) = '0' AND REG_OUT(43 DOWNTO 40) /= "0000" ELSE
     X"80000000" WHEN REG_OUT(44) = '1' AND REG_OUT(43 DOWNTO 40) /= "1111" ELSE
     REG_OUT(40 DOWNTO 9);
OUTPUT_REG: REGNE
    GENERIC MAP(N=>32)
    PORT MAP(CLOCK  => CLOCK,
        RESET   => RESET,
        CLEAR   => '1',
        EN  => '1',
        INPUT   => O_INT,
        OUTPUT  => O
        );
END ARCHITECTURE BEHAVIOR;
-----------------------------------end of iirK filter for 32 bits---------------------------------

----------------------------------------IIR9 SIMPLE FILTER-------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY IIR924_SIMPLE IS
    PORT(CLOCK  : IN STD_LOGIC;
         RESET  : IN STD_LOGIC;
         LOAD   : IN STD_LOGIC;
         I      : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         O      : OUT STD_LOGIC_VECTOR(23 DOWNTO 0)
         );
END ENTITY IIR924_SIMPLE;
ARCHITECTURE BEHAVIOR OF IIR924_SIMPLE IS
SIGNAL I_EXT        : STD_LOGIC_VECTOR(36 DOWNTO 0);
SIGNAL REG_SHIFT    : STD_LOGIC_VECTOR(36 DOWNTO 0);
SIGNAL REG      : STD_LOGIC_VECTOR(36 DOWNTO 0);
SIGNAL ONE      : STD_LOGIC;
SIGNAL REG_IN       : STD_LOGIC_VECTOR(36 DOWNTO 0);
SIGNAL REG_OUT      : STD_LOGIC_VECTOR(36 DOWNTO 0);
SIGNAL O_INT        : STD_LOGIC_VECTOR(23 DOWNTO 0);
BEGIN
I_EXT(36 DOWNTO 24) <= (OTHERS => I(23));
I_EXT(23 DOWNTO 0) <= I;
FILTER_REG: REGNE
    GENERIC MAP(N => 37)
    PORT MAP(CLOCK  => CLOCK,
        RESET   => RESET,
        CLEAR   => '1',
        EN  => LOAD,
        INPUT   => REG_IN,
        OUTPUT  => REG_OUT
        );

REG_IN  <= REG_OUT - REG_SHIFT + I_EXT;

REG_SHIFT(36 DOWNTO 28) <= (OTHERS => REG_OUT(36));
REG_SHIFT(27 DOWNTO 0) <= REG_OUT(36 DOWNTO 9);
O_INT <= X"7FFFFF" WHEN REG_OUT(36) = '0' AND REG_OUT(35 DOWNTO 32) /= "0000" ELSE
     X"800000" WHEN REG_OUT(36) = '1' AND REG_OUT(35 DOWNTO 32) /= "1111" ELSE
     REG_OUT(32 DOWNTO 9);
OUTPUT_REG: REGNE
    GENERIC MAP(N=>24)
    PORT MAP(CLOCK  => CLOCK,
        RESET   => RESET,
        CLEAR   => '1',
        EN  => '1',
        INPUT   => O_INT,
        OUTPUT  => O
        );
END ARCHITECTURE BEHAVIOR;
-----------------------------------end of iirK filter for 24 bits----------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY DAC8568_CONTROL IS

PORT(CLOCK : IN STD_LOGIC;
     RESET : IN STD_LOGIC;
     DATA0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA3 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA4 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA5 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA6 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA7 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);


     DATA0_DAC2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA1_DAC2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA2_DAC2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA3_DAC2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA4_DAC2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA5_DAC2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA6_DAC2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
     DATA7_DAC2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);

     SCLK : OUT STD_LOGIC;
     DIN1 : OUT STD_LOGIC;
     DIN2 : OUT STD_LOGIC;
     SYNC : OUT STD_LOGIC


     );
END ENTITY DAC8568_CONTROL;

ARCHITECTURE BEHAVIOR OF DAC8568_CONTROL IS


----data stream for enabling internal reference
--0xxx_1001_xxxx_110x_xxxx_xxxx_xxxx_xxxx
SIGNAL DAC_DATA_BUF : REG16_8;

SIGNAL DAC_DATA_IN  : REG16_8;

SIGNAL DAC2_DATA_BUF    : REG16_8;
SIGNAL DAC2_DATA_IN : REG16_8;

signal REF_EN : std_logic_vector(31 downto 0);

SIGNAL LOAD_DAC_REG : STD_LOGIC;
SIGNAL DAC_REG_EN : STD_LOGIC;
SIGNAL DAC_REG_IN : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL DAC2_REG_IN : STD_LOGIC_VECTOR(31 DOWNTO 0);

SIGNAL EN_DAC_DATA : STD_LOGIC;
SIGNAL CLR_DAC_DATA : STD_LOGIC;
SIGNAL DAC_DATA : STD_LOGIC;
SIGNAL DATA_IN : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL DATA2_IN : STD_LOGIC_VECTOR(31 DOWNTO 0);

SIGNAL EN_DAC_CNT : STD_LOGIC;
SIGNAL DAC_CNT : STD_LOGIC_VECTOR(2 DOWNTO 0);

SIGNAL EN_SCLK_CNT : STD_LOGIC;
SIGNAL CLR_SCLK_CNT : STD_LOGIC;
SIGNAL SCLK_CNT : STD_LOGIC_VECTOR(4 DOWNTO 0);

SIGNAL EN_SYNC_CNT : STD_LOGIC;
SIGNAL SYNC_CNT : STD_LOGIC_VECTOR(7 DOWNTO 0);

SIGNAL EN_SCLK_DIV_CNT  : STD_LOGIC;
SIGNAL CLR_SCLK_DIV_CNT : STD_LOGIC;
SIGNAL SCLK_DIV_CNT     : STD_LOGIC_VECTOR(9 DOWNTO 0);

SIGNAL EN_WAIT_CNT  : STD_LOGIC;
SIGNAL WAIT_CNT     : STD_LOGIC_VECTOR(15 DOWNTO 0);

SIGNAL ONE  : STD_LOGIC;
SIGNAL DAC_DIN  : STD_LOGIC;

SIGNAL DAC2_DIN : STD_LOGIC;

TYPE STATE_TYPE IS (INIT, REF_ENABLE, SCLK_REF_HIGH, SCLK_REF_LOW, DATA_INIT, DATA_LOAD, SCLK_DATA_HIGH, SCLK_DATA_LOW, TWAIT, DATA_NEW_WAIT);
SIGNAL STATE : STATE_TYPE;

SIGNAL WRITE_PENDING, WRITE_PENDING_2 : STD_LOGIC_VECTOR(7 downto 0) := (others => '1');

BEGIN

ONE <= '1';

REF_EN <= "00001001000010100000000000000000";

DAC_DATA_IN(0) <= DATA0;
DAC_DATA_IN(1) <= DATA1;
DAC_DATA_IN(2) <= DATA2;
DAC_DATA_IN(3) <= DATA3;
DAC_DATA_IN(4) <= DATA4;
DAC_DATA_IN(5) <= DATA5;
DAC_DATA_IN(6) <= DATA6;
DAC_DATA_IN(7) <= DATA7;

DAC2_DATA_IN(0) <= DATA0_DAC2;
DAC2_DATA_IN(1) <= DATA1_DAC2;
DAC2_DATA_IN(2) <= DATA2_DAC2;
DAC2_DATA_IN(3) <= DATA3_DAC2;
DAC2_DATA_IN(4) <= DATA4_DAC2;
DAC2_DATA_IN(5) <= DATA5_DAC2;
DAC2_DATA_IN(6) <= DATA6_DAC2;
DAC2_DATA_IN(7) <= DATA7_DAC2;
--data1 <= "00000000000011111111111111110000";
--data2 <= "00000000000111100110011001010000";
--data3 <= "00000000001011001100110011000000";
--data4 <= "00000000001110110011001100100000";
--data5 <= "00000000010010011001100110010000";
--data6 <= "00000000010101111111111111110000";
--data7 <= "00000000011001100110011001100000";
--data8 <= "00000000011100110011001100110000";

DAC_DATA_REG_GEN_I: FOR I IN 0 TO 7 GENERATE
    DAC_DATA_REGI: REGNE
            GENERIC MAP(N => 16)
            PORT MAP(CLOCK      => CLOCK,
                        RESET       => RESET,
                        CLEAR       => '1',
                        EN      => '1',
                        INPUT       => DAC_DATA_IN(I),
                        OUTPUT  => DAC_DATA_BUF(I)
                        );
END GENERATE;

DAC_DATA2_REG_GEN_I: FOR I IN 0 TO 7 GENERATE
    DAC_DATA2_REGI: REGNE
            GENERIC MAP(N => 16)
            PORT MAP(CLOCK      => CLOCK,
                        RESET       => RESET,
                        CLEAR       => '1',
                        EN      => '1',
                        INPUT       => DAC2_DATA_IN(I),
                        OUTPUT  => DAC2_DATA_BUF(I)
                        );
END GENERATE;
DAC_DATA_LATCH: FLIP_FLOP
    PORT MAP(CLOCK => CLOCK,
             RESET => RESET,
             CLEAR => CLR_DAC_DATA,
             EN    => EN_DAC_DATA,
             INP   => ONE,
             OUP   => DAC_DATA
            );

INIT_WAIT_CNTR: COUNTER
        GENERIC MAP(N => 16)
        PORT MAP(CLOCK  => CLOCK,
                 RESET  => RESET,
                 CLEAR  => ONE,
                 ENABLE => EN_WAIT_CNT,
                 COUNT  => WAIT_CNT
                );
SCLK_CNTR: COUNTER
        GENERIC MAP(N => 5)
        PORT MAP(CLOCK  => CLOCK,
                 RESET  => RESET,
                 CLEAR  => CLR_SCLK_CNT,
                 ENABLE => EN_SCLK_CNT,
                 COUNT  => SCLK_CNT
                );

SYNC_CNTR: COUNTER
        GENERIC MAP(N => 8)
        PORT MAP(CLOCK  => CLOCK,
                 RESET  => RESET,
                 CLEAR  => ONE,
                 ENABLE => EN_SYNC_CNT,
                 COUNT  => SYNC_CNT
                );

SCLK_DIV_CNTR: COUNTER
        GENERIC MAP(N => 10)
        PORT MAP(CLOCK  => CLOCK,
                 RESET  => RESET,
                 CLEAR  => CLR_SCLK_DIV_CNT,
                 ENABLE => EN_SCLK_DIV_CNT,
                 COUNT  => SCLK_DIV_CNT
                );


DAC_ADDR_CNTR: COUNTER
        GENERIC MAP(N => 3)
        PORT MAP(CLOCK  => CLOCK,
                 RESET  => RESET,
                 CLEAR  => ONE,
                 ENABLE => EN_DAC_CNT,
                 COUNT  => DAC_CNT
                );

    -- Keep track of pending writes
    PROCESS(CLOCK, RESET)
    BEGIN
        IF (RESET = '0') THEN
            WRITE_PENDING   <= (OTHERS => '1');
            WRITE_PENDING_2 <= (OTHERS => '1');
        ELSIF (CLOCK = '1' AND CLOCK'EVENT) THEN
            FOR I IN 0 TO 7 LOOP
                IF (DAC_DATA_IN(I) /= DAC_DATA_BUF(I) AND
                    NOT (LOAD_DAC_REG = '1' AND DAC_CNT = I)) THEN
                    WRITE_PENDING(I) <= '1';
                ELSIF (LOAD_DAC_REG = '1' AND DAC_CNT = I) THEN
                    WRITE_PENDING(I) <= '0';
                END IF;

                IF (DAC2_DATA_IN(I) /= DAC2_DATA_BUF(I) AND
                    NOT (LOAD_DAC_REG = '1' AND DAC_CNT = I)) THEN
                    WRITE_PENDING_2(I) <= '1';
                ELSIF (LOAD_DAC_REG = '1' AND DAC_CNT = I) THEN
                    WRITE_PENDING_2(I) <= '0';
                END IF;
            END LOOP;
        END IF;
    END PROCESS;


DAC_REG_IN <= DATA_IN WHEN DAC_DATA = '1' ELSE REF_EN;

DATA_IN <= "000000000000" & DAC_DATA_BUF(0) & "0000" WHEN DAC_CNT = "000" ELSE
           "000000000001" & DAC_DATA_BUF(1) & "0000" WHEN DAC_CNT = "001" ELSE
           "000000000010" & DAC_DATA_BUF(2) & "0000" WHEN DAC_CNT = "010" ELSE
           "000000000011" & DAC_DATA_BUF(3) & "0000" WHEN DAC_CNT = "011" ELSE
           "000000000100" & DAC_DATA_BUF(4) & "0000" WHEN DAC_CNT = "100" ELSE
           "000000000101" & DAC_DATA_BUF(5) & "0000" WHEN DAC_CNT = "101" ELSE
           "000000000110" & DAC_DATA_BUF(6) & "0000" WHEN DAC_CNT = "110" ELSE
           "000000000111" & DAC_DATA_BUF(7) & "0000" WHEN DAC_CNT = "111" ELSE
           (OTHERS => '0');

DAC_DATA_IN_REG: SHIFT_REG
    GENERIC MAP(N => 32)
    PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
             EN     => DAC_REG_EN,
             LOAD   => LOAD_DAC_REG,
             INP    => DAC_REG_IN,
             OUTPUT => DAC_DIN
            );
DAC2_REG_IN <= DATA2_IN WHEN DAC_DATA = '1' ELSE REF_EN;


DATA2_IN <= "000000000000" & DAC2_DATA_BUF(0) & "0000" WHEN DAC_CNT = "000" ELSE
           "000000000001" & DAC2_DATA_BUF(1) & "0000" WHEN DAC_CNT = "001" ELSE
           "000000000010" & DAC2_DATA_BUF(2) & "0000" WHEN DAC_CNT = "010" ELSE
           "000000000011" & DAC2_DATA_BUF(3) & "0000" WHEN DAC_CNT = "011" ELSE
           "000000000100" & DAC2_DATA_BUF(4) & "0000" WHEN DAC_CNT = "100" ELSE
           "000000000101" & DAC2_DATA_BUF(5) & "0000" WHEN DAC_CNT = "101" ELSE
           "000000000110" & DAC2_DATA_BUF(6) & "0000" WHEN DAC_CNT = "110" ELSE
           "000000000111" & DAC2_DATA_BUF(7) & "0000" WHEN DAC_CNT = "111" ELSE
           (OTHERS => '0');

DAC2_DATA_IN_REG: SHIFT_REG
    GENERIC MAP(N => 32)
    PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
             EN     => DAC_REG_EN,
             LOAD   => LOAD_DAC_REG,
             INP    => DAC2_REG_IN,
             OUTPUT => DAC2_DIN
            );

DIN1 <= DAC_DIN;
DIN2 <= DAC2_DIN;

    PROCESS(CLOCK, RESET)
    BEGIN

        IF (RESET = '0') THEN
            STATE <= INIT;
        ELSIF (CLOCK = '1' AND CLOCK'EVENT) THEN

            CASE STATE IS

                WHEN INIT => IF (WAIT_CNT = x"FFFF") THEN STATE <= REF_ENABLE;
                         ELSE STATE <= INIT;
                         END IF;

                WHEN REF_ENABLE => STATE <= SCLK_REF_HIGH;

                WHEN SCLK_REF_HIGH => IF SCLK_DIV_CNT = "0111111111" THEN STATE <= SCLK_REF_LOW;
                                             ELSE STATE <= SCLK_REF_HIGH;
                                             END IF;

                WHEN SCLK_REF_LOW => IF SCLK_DIV_CNT = "1111111111" THEN
                                                IF SCLK_CNT = "11111" THEN STATE <= DATA_INIT;
                                                ELSE STATE <= SCLK_REF_HIGH;
                                                END IF;
                                            ELSE STATE <= SCLK_REF_LOW;
                                            END IF;

                WHEN DATA_INIT => IF(SYNC_CNT = "11111111") THEN STATE <= DATA_LOAD;
                                  ELSE STATE <= DATA_INIT;
                                  END IF;

                WHEN DATA_LOAD => STATE <= SCLK_DATA_HIGH;

                WHEN SCLK_DATA_HIGH => IF SCLK_DIV_CNT = "0111111111" THEN STATE <= SCLK_DATA_LOW;
                                              ELSE STATE <= SCLK_DATA_HIGH;
                                              END IF;

                WHEN SCLK_DATA_LOW => IF SCLK_DIV_CNT = "1111111111" THEN
                                                IF(SCLK_CNT = "11111") THEN STATE <= TWAIT;
                                                ELSE STATE <= SCLK_DATA_HIGH;
                                                END IF;
                                             ELSE STATE <= SCLK_DATA_LOW;
                                             END IF;

                WHEN TWAIT => IF DAC_CNT = "111" THEN STATE <= DATA_NEW_WAIT;
                                    ELSE STATE <= DATA_INIT;
                                    END IF;

                WHEN DATA_NEW_WAIT  => IF (WRITE_PENDING = x"0000" AND WRITE_PENDING_2 = x"0000") THEN STATE <= DATA_NEW_WAIT;
                                                ELSE STATE <= DATA_INIT;
                                                END IF;

            END CASE;
        END IF;
    END PROCESS;

SYNC <= '0' WHEN STATE = REF_ENABLE OR STATE = SCLK_REF_HIGH OR STATE = SCLK_REF_LOW OR STATE = DATA_LOAD OR
            STATE = SCLK_DATA_HIGH OR STATE = SCLK_DATA_LOW OR STATE = TWAIT ELSE '1';

SCLK <= '0' WHEN STATE = SCLK_REF_LOW OR STATE = SCLK_DATA_LOW ELSE '1';

DAC_REG_EN <= '1' WHEN (STATE = SCLK_DATA_LOW AND SCLK_DIV_CNT = "1111111011") OR (STATE = SCLK_REF_LOW AND SCLK_DIV_CNT = "1111111011") ELSE '0';

EN_SYNC_CNT <= '1' WHEN (STATE = DATA_INIT) ELSE '0';

EN_SCLK_CNT <= '1' WHEN (STATE = SCLK_REF_LOW AND SCLK_DIV_CNT = "1111111111") OR (STATE = SCLK_DATA_LOW AND SCLK_DIV_CNT = "1111111111") ELSE '0';
CLR_SCLK_CNT    <= '0' WHEN STATE = DATA_LOAD ELSE '1';

EN_SCLK_DIV_CNT <= '1' WHEN STATE = SCLK_REF_LOW OR STATE = SCLK_REF_HIGH OR STATE = SCLK_DATA_HIGH OR STATE = SCLK_DATA_LOW ELSE '0';
CLR_SCLK_DIV_CNT    <= '0' WHEN STATE = DATA_LOAD ELSE '1';


EN_DAC_CNT <= '1' WHEN STATE = TWAIT ELSE '0';

LOAD_DAC_REG <= '1' WHEN STATE = REF_ENABLE OR STATE = DATA_LOAD ELSE '0';

EN_DAC_DATA <= '1' WHEN (STATE = SCLK_REF_LOW AND SCLK_CNT = "11111") ELSE '0';
CLR_DAC_DATA <= '0' WHEN STATE = INIT ELSE '1';

EN_WAIT_CNT <= '1' WHEN STATE = INIT ELSE '0';


END ARCHITECTURE BEHAVIOR;
------------------------------------------ADS1258 for temperature sensors----------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY ADS1258 IS

PORT(CLOCK      : IN STD_LOGIC;
      RESET     : IN STD_LOGIC;

      DOUT      : IN STD_LOGIC;

      CS        : OUT STD_LOGIC;
      SCLK      : OUT STD_LOGIC;
      DIN       : OUT STD_LOGIC;
      LD_DATA   : OUT STD_LOGIC;

      DATA_OUT0     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT1     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT2     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT3     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT4     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT5     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT6     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT7     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT8     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT9     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT10    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT11    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT12    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT13    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT14    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      DATA_OUT15    : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
      );

END ENTITY ADS1258;

ARCHITECTURE BEHAVIOR OF ADS1258 IS

SIGNAL EN_SCLK_COUNT    : STD_LOGIC;
SIGNAL CLR_SCLK_COUNT   : STD_LOGIC;
SIGNAL SCLK_COUNT       : STD_LOGIC_VECTOR(7 DOWNTO 0);

SIGNAL EN_CS_COUNT      : STD_LOGIC;
SIGNAL CLR_CS_COUNT     : STD_LOGIC;
SIGNAL CS_COUNT         : STD_LOGIC_VECTOR(9 DOWNTO 0);

SIGNAL EN_BIT_COUNT     : STD_LOGIC;
SIGNAL CLR_BIT_COUNT    : STD_LOGIC;
SIGNAL BIT_COUNT        : STD_LOGIC_VECTOR(5 DOWNTO 0);

SIGNAL EN_DATA_SHIFT    : STD_LOGIC;
SIGNAL LOAD_DATA_SHIFT  : STD_LOGIC;
SIGNAL ADC_DATA         : STD_LOGIC_VECTOR(31 DOWNTO 0);

SIGNAL EN_DATA_REG      : STD_LOGIC_VECTOR(15 DOWNTO 0);

SIGNAL DIN_BUF          : STD_LOGIC;
SIGNAL DIN_BUF0         : STD_LOGIC;
SIGNAL DIN_BUF1         : STD_LOGIC;
SIGNAL EN_SHIFT_DIN     : STD_LOGIC;
SIGNAL LD_SHIFT_DIN     : STD_LOGIC;
SIGNAL EN_SHIFT_DIN0        : STD_LOGIC;
SIGNAL LD_SHIFT_DIN0        : STD_LOGIC;

SIGNAL DRDY_BUF0        : STD_LOGIC;
SIGNAL DRDY_BUF1        : STD_LOGIC;
SIGNAL DRDY_BUF2        : STD_LOGIC;

SIGNAL DRDY_LOW         : STD_LOGIC;

SIGNAL CS_OUT           : STD_LOGIC;

SIGNAL DOUT_BUF2            : STD_LOGIC;

SIGNAL SCLK_OUT         : STD_LOGIC;
SIGNAL DATA_OUT         : REG24_16;

TYPE STATE_TYPE IS (INIT, CS_HIGH, CS_LOW, SCLK_HIGH_DIN, SCLK_LOW_DIN, SCLK_HIGH, SCLK_LOW, DATA_ACQ);
SIGNAL STATE        : STATE_TYPE;

BEGIN

CS_FF: LATCH_N
    PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
             CLEAR  => '1',
             EN     => '1',
             INP    => CS_OUT,
             OUP    => CS
            );

DIN_FF: LATCH_N
    PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
             CLEAR  => '1',
             EN     => '1',
             INP    => DIN_BUF,
             OUP    => DIN
            );

DOUT_FF: LATCH_N
    PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
             CLEAR  => '1',
             EN     => '1',
             INP    => DOUT,
             OUP    => DOUT_BUF2
            );

SCLK_FF: LATCH_N
    PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
             CLEAR  => '1',
             EN     => '1',
             INP    => SCLK_OUT,
             OUP    => SCLK
            );


SCLK_COUNTER: COUNTER
        GENERIC MAP(N => 8)
        PORT MAP(CLOCK      => CLOCK,
                 RESET      => RESET,
                 CLEAR      => CLR_SCLK_COUNT,
                 ENABLE     => EN_SCLK_COUNT,
                 COUNT      => SCLK_COUNT
                );

BIT_COUNTER: COUNTER
        GENERIC MAP(N => 6)
        PORT MAP(CLOCK      => CLOCK,
                 RESET      => RESET,
                 CLEAR      => CLR_BIT_COUNT,
                 ENABLE     => EN_BIT_COUNT,
                 COUNT      => BIT_COUNT
                );

CS_COUNTER: COUNTER
        GENERIC MAP(N => 10)
        PORT MAP(CLOCK      => CLOCK,
                 RESET      => RESET,
                 CLEAR      => CLR_CS_COUNT,
                 ENABLE     => EN_CS_COUNT,
                 COUNT      => CS_COUNT
                );

DIN_DATA_REG1: SHIFT_REG
    GENERIC MAP(N => 8)
    PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
             EN     => EN_SHIFT_DIN,
             LOAD   => LD_SHIFT_DIN,
             INP    => "00110000",
             OUTPUT => DIN_BUF
            );

DATA_SHIFT_REG: SHIFT_LEFT_BIT
    GENERIC MAP(N => 32)
    PORT MAP(CLOCK      => CLOCK,
             RESET      => RESET,
             EN         => EN_DATA_SHIFT,
             LOAD       => LOAD_DATA_SHIFT,
             INP        => X"00000000",
             SHIFT_BIT  => DOUT_BUF2,
             OUTPUT     => ADC_DATA
            );

GEN_DATA_REG_I: FOR I IN 0 TO 15 GENERATE

DATA_REG_I: REGNE
        GENERIC MAP(N => 24)
        PORT MAP(CLOCK  => CLOCK,
                 RESET  => RESET,
                 CLEAR  => '1',
                 EN     => EN_DATA_REG(I),
                 INPUT  => ADC_DATA(23 DOWNTO 0),
                 OUTPUT => DATA_OUT(I)
                );

END GENERATE;

DATA_OUT0 <= (x"00" & DATA_OUT(0)) WHEN DATA_OUT(0)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT1 <= (x"00" & DATA_OUT(1)) WHEN DATA_OUT(1)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT2 <= (x"00" & DATA_OUT(2)) WHEN DATA_OUT(2)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT3 <= (x"00" & DATA_OUT(3)) WHEN DATA_OUT(3)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT4 <= (x"00" & DATA_OUT(4)) WHEN DATA_OUT(4)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT5 <= (x"00" & DATA_OUT(5)) WHEN DATA_OUT(5)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT6 <= (x"00" & DATA_OUT(6)) WHEN DATA_OUT(6)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT7 <= (x"00" & DATA_OUT(7)) WHEN DATA_OUT(7)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT8 <= (x"00" & DATA_OUT(8)) WHEN DATA_OUT(8)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT9 <= (x"00" & DATA_OUT(9)) WHEN DATA_OUT(9)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT10 <= (x"00" & DATA_OUT(10)) WHEN DATA_OUT(10)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT11 <= (x"00" & DATA_OUT(11)) WHEN DATA_OUT(11)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT12 <= (x"00" & DATA_OUT(12)) WHEN DATA_OUT(12)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT13 <= (x"00" & DATA_OUT(13)) WHEN DATA_OUT(13)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT14 <= (x"00" & DATA_OUT(14)) WHEN DATA_OUT(14)(23) = '0' ELSE (OTHERS => '0');
DATA_OUT15 <= (x"00" & DATA_OUT(15)) WHEN DATA_OUT(15)(23) = '0' ELSE (OTHERS => '0');




EN_DATA_REG(0)  <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "01000" ELSE '0';
EN_DATA_REG(1)  <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "01001" ELSE '0';
EN_DATA_REG(2)  <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "01010" ELSE '0';
EN_DATA_REG(3)  <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "01011" ELSE '0';
EN_DATA_REG(4)  <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "01100" ELSE '0';
EN_DATA_REG(5)  <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "01101" ELSE '0';
EN_DATA_REG(6)  <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "01110" ELSE '0';
EN_DATA_REG(7)  <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "01111" ELSE '0';
EN_DATA_REG(8)  <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "10000" ELSE '0';
EN_DATA_REG(9)  <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "10001" ELSE '0';
EN_DATA_REG(10) <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "10010" ELSE '0';
EN_DATA_REG(11) <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "10011" ELSE '0';
EN_DATA_REG(12) <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "10100" ELSE '0';
EN_DATA_REG(13) <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "10101" ELSE '0';
EN_DATA_REG(14) <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "10110" ELSE '0';
EN_DATA_REG(15) <= '1' WHEN STATE = DATA_ACQ AND ADC_DATA(28 DOWNTO 24) = "10111" ELSE '0';


PROCESS(RESET, CLOCK)
BEGIN
    IF (RESET = '0') THEN
        STATE <= INIT;
    ELSIF (CLOCK = '1' AND CLOCK'EVENT) THEN

        CASE STATE IS

            WHEN INIT           =>  STATE <= CS_HIGH;

            WHEN CS_HIGH        =>  IF CS_COUNT = "1111111111" THEN STATE <= CS_LOW;
                                    ELSE STATE <= CS_HIGH;
                                    END IF;

            WHEN CS_LOW         =>  IF CS_COUNT = "1111111111" THEN STATE <= SCLK_LOW_DIN;
                                    ELSE STATE <= CS_LOW;
                                    END IF;

            WHEN SCLK_LOW_DIN   =>  IF SCLK_COUNT = "01111111" THEN STATE <= SCLK_HIGH_DIN;
                                    ELSE STATE <= SCLK_LOW_DIN;
                                    END IF;

            WHEN SCLK_HIGH_DIN  =>  IF SCLK_COUNT = "11111111" THEN
                                        IF BIT_COUNT = "000111" THEN STATE <= SCLK_LOW;
                                        ELSE STATE <= SCLK_LOW_DIN;
                                        END IF;
                                    ELSE STATE <= SCLK_HIGH_DIN;
                                    END IF;

            WHEN SCLK_LOW       =>  IF SCLK_COUNT = "01111111" THEN STATE <= SCLK_HIGH;
                                    ELSE STATE <= SCLK_LOW;
                                    END IF;

            WHEN SCLK_HIGH      =>  IF SCLK_COUNT = "11111111" THEN
                                        IF BIT_COUNT = "011111" THEN STATE <= DATA_ACQ;
                                        ELSE STATE <= SCLK_LOW;
                                        END IF;
                                    ELSE STATE <= SCLK_HIGH;
                                    END IF;

            WHEN DATA_ACQ       => STATE <= CS_HIGH;

        END CASE;

    END IF;
END PROCESS;

EN_CS_COUNT <= '1' WHEN (STATE = CS_HIGH OR STATE = CS_LOW) ELSE '0';
CLR_CS_COUNT    <= '0' WHEN (STATE = CS_HIGH AND  CS_COUNT = "1111111111") OR STATE = DATA_ACQ ELSE '1';

EN_SCLK_COUNT   <= '1' WHEN STATE = SCLK_HIGH_DIN OR STATE = SCLK_LOW_DIN OR STATE = SCLK_HIGH OR STATE = SCLK_LOW ELSE '0';
EN_BIT_COUNT    <= '1' WHEN (STATE = SCLK_HIGH AND SCLK_COUNT = "11111111" AND BIT_COUNT /= "011111") OR (STATE = SCLK_HIGH_DIN AND SCLK_COUNT = "11111111" AND BIT_COUNT /= "000111") ELSE '0';


CLR_SCLK_COUNT  <= '0' WHEN STATE = DATA_ACQ ELSE '1';
CLR_BIT_COUNT   <= '0' WHEN STATE = DATA_ACQ OR (STATE = SCLK_HIGH_DIN AND SCLK_COUNT = "11111111" AND BIT_COUNT = "000111") ELSE '1';

LOAD_DATA_SHIFT <= '1' WHEN STATE = CS_LOW AND CS_COUNT = "1111111111" ELSE '0';
EN_DATA_SHIFT   <= '1' WHEN STATE = SCLK_HIGH AND SCLK_COUNT = "10001111" ELSE '0';

LD_SHIFT_DIN    <= '1' WHEN STATE = CS_LOW AND CS_COUNT = "1111111111" ELSE '0';
EN_SHIFT_DIN    <= '1' WHEN STATE = SCLK_HIGH_DIN AND SCLK_COUNT = "11111111" ELSE '0';

SCLK_OUT    <= '1' WHEN STATE = SCLK_HIGH OR STATE = SCLK_HIGH_DIN ELSE '0';
CS_OUT      <= '1' WHEN STATE = CS_HIGH ELSE '0';
LD_DATA     <= '1' WHEN STATE = DATA_ACQ ELSE '0';


END ARCHITECTURE BEHAVIOR;

-------------------PT100 temperature compare-------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY PT100_COMPARE IS
    PORT(CLOCK : IN STD_LOGIC;
         RESET : IN STD_LOGIC;
         TMP_DATA0 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA1 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA2 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA3 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA4 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA5 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA6 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_DATA7 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT0 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT1 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT2 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT3 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT4 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT5 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT6 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LIMIT7 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_DISABLE : IN STD_LOGIC; -- Low-temperature check chicken-bit
         TMP_LO_LIMIT0 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT1 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT2 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT3 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT4 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT5 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT6 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         TMP_LO_LIMIT7 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
         MASK_TMP_FAULT : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         FLT_CLR : IN STD_LOGIC;
         TMP_FAULT_CLEAR : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
         LD_DATA : IN STD_LOGIC;
         TMP_FAULT : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         TMP_STATUS : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
         );
END ENTITY PT100_COMPARE;

ARCHITECTURE BEHAVIOR OF PT100_COMPARE IS

SIGNAL TMP_HI_TEST_FLT     : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL TMP_LO_TEST_FLT     : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL TMP_CHECK           : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL TMP_DATA_INT        : REG24_8;
SIGNAL TMP_DATA_INT_FLT    : REG24_8;
SIGNAL TMP_LIMIT_INT       : REG24_8;
SIGNAL TMP_LO_LIMIT_INT    : REG24_8;
SIGNAL TMP_FAULT_CLEAR_INT : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL ONE                 : STD_LOGIC;


BEGIN

TMP_DATA_INT(0) <= TMP_DATA0;
TMP_DATA_INT(1) <= TMP_DATA1;
TMP_DATA_INT(2) <= TMP_DATA2;
TMP_DATA_INT(3) <= TMP_DATA3;
TMP_DATA_INT(4) <= TMP_DATA4;
TMP_DATA_INT(5) <= TMP_DATA5;
TMP_DATA_INT(6) <= TMP_DATA6;
TMP_DATA_INT(7) <= TMP_DATA7;
TMP_LIMIT_INT(0) <= TMP_LIMIT0;
TMP_LIMIT_INT(1) <= TMP_LIMIT1;
TMP_LIMIT_INT(2) <= TMP_LIMIT2;
TMP_LIMIT_INT(3) <= TMP_LIMIT3;
TMP_LIMIT_INT(4) <= TMP_LIMIT4;
TMP_LIMIT_INT(5) <= TMP_LIMIT5;
TMP_LIMIT_INT(6) <= TMP_LIMIT6;
TMP_LIMIT_INT(7) <= TMP_LIMIT7;
TMP_LO_LIMIT_INT(0) <= TMP_LO_LIMIT0;
TMP_LO_LIMIT_INT(1) <= TMP_LO_LIMIT1;
TMP_LO_LIMIT_INT(2) <= TMP_LO_LIMIT2;
TMP_LO_LIMIT_INT(3) <= TMP_LO_LIMIT3;
TMP_LO_LIMIT_INT(4) <= TMP_LO_LIMIT4;
TMP_LO_LIMIT_INT(5) <= TMP_LO_LIMIT5;
TMP_LO_LIMIT_INT(6) <= TMP_LO_LIMIT6;
TMP_LO_LIMIT_INT(7) <= TMP_LO_LIMIT7;

ONE <= '1';

TMP_DATA_FILTER_GEN_I: FOR I IN 0 TO 7 GENERATE
TMP_DATA_IIR_REG_I: IIR924_SIMPLE
        PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
             LOAD   => LD_DATA,
             I  => TMP_DATA_INT(I),
             O  => TMP_DATA_INT_FLT(I)
            );
END GENERATE;



GEN_TMP_FAULT: FOR I IN 0 TO 7 GENERATE

TMP_HI_TEST_FLT(I) <= '1' WHEN ((TMP_DATA_INT_FLT(I)(23 DOWNTO 0) > TMP_LIMIT_INT(I)(23 DOWNTO 0)) AND
                                (TMP_DATA_INT_FLT(I)(23) = TMP_LIMIT_INT(I)(23))) OR
                               (TMP_DATA_INT_FLT(I)(23) = '0' AND TMP_LIMIT_INT(I)(23) = '1') ELSE '0';

TMP_LO_TEST_FLT(I) <= (NOT TMP_LO_DISABLE) WHEN ((TMP_DATA_INT_FLT(I)(23 DOWNTO 0) < TMP_LO_LIMIT_INT(I)(23 DOWNTO 0)) AND
                                                 (TMP_DATA_INT_FLT(I)(23) = TMP_LO_LIMIT_INT(I)(23))) OR
                                                (TMP_DATA_INT_FLT(I)(23) = '1' AND TMP_LO_LIMIT_INT(I)(23) = '0') ELSE '0';

TMP_CHECK(I)  <= (TMP_HI_TEST_FLT(I) OR TMP_LO_TEST_FLT(I)) AND NOT MASK_TMP_FAULT(I);
TMP_STATUS(I) <= (TMP_HI_TEST_FLT(I) OR TMP_LO_TEST_FLT(I)); -- Non-masked Instantaneous fault indicator


    TMP_FAULT_LATCHI: LATCH_N
    PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
             CLEAR  => TMP_FAULT_CLEAR_INT(I),
             EN     => TMP_CHECK(I),
             INP    => ONE,
             OUP    => TMP_FAULT(I)
            );

TMP_FAULT_CLEAR_INT(I) <= NOT (TMP_FAULT_CLEAR(I) OR FLT_CLR);
END GENERATE;

END ARCHITECTURE BEHAVIOR;
--------------------------end of PT100 temperature compare--------------------

----------------------------dig I/O stat and control-------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.components.all;

entity dig_in_out is
port(clock      : in std_logic;
    reset       : in std_logic;

    dig_in1     : in std_logic;
    dig_in2     : in std_logic;
    dig_in3     : in std_logic;

    dig_out_epics   : in std_logic_vector(6 downto 0);

    dig_in_stat : out std_logic_vector(2 downto 0);
    dig_out1    : out std_logic;
    dig_out2    : out std_logic;
    dig_out3    : out std_logic;
    dig_out4    : out std_logic;
    dig_out5    : out std_logic;
    dig_out6    : out std_logic;
    dig_out7    : out std_logic
    );
end entity dig_in_out;
architecture behavior of dig_in_out is
signal dig_out_buf  : std_logic_vector(6 downto 0);
signal dig_in_buf   : std_logic_vector(2 downto 0);
begin

dig_out_register: REGNE
        generic map(n=>7)
        port map(clock      => clock,
              reset     => reset,
              clear     => '1',
              en        => '1',
              input     => dig_out_epics,
              output    => dig_out_buf
              );
dig_out1    <= dig_out_buf(0);
dig_out2    <= dig_out_buf(1);
dig_out3    <= dig_out_buf(2);
dig_out4    <= dig_out_buf(3);
dig_out5    <= dig_out_buf(4);
dig_out6    <= dig_out_buf(5);
dig_out7    <= dig_out_buf(6);

dig_in_buf(0)   <= dig_in1;
dig_in_buf(1)   <= dig_in2;
dig_in_buf(2)   <= dig_in3;


dig_in_register: REGNE
        generic map(n=>3)
        port map(clock      => clock,
              reset     => reset,
              clear     => '1',
              en        => '1',
              input     => dig_in_buf,
              output    => dig_in_stat
              );
end architecture behavior;
----------------------end of dig I/O stat and control--------------------------
---------------------vac fault status-----------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY VAC_FAULT_STATUS IS
PORT(CLOCK : IN STD_LOGIC;
     RESET : IN STD_LOGIC;
     VAC_IN0 : IN STD_LOGIC;
     VAC_IN1 : IN STD_LOGIC;
     MASK_VAC : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
     FLT_CLR : IN STD_LOGIC;
     VAC_CLR : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
     VAC_FLT : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
     VAC_STATUS : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
     );
END ENTITY VAC_FAULT_STATUS;
ARCHITECTURE BEHAVIOR OF VAC_FAULT_STATUS IS
SIGNAL EN_VAC_FLT0 : STD_LOGIC;
SIGNAL EN_VAC_FLT1 : STD_LOGIC;
SIGNAL VAC_CLR_INT : STD_LOGIC_VECTOR(1 DOWNTO 0);
BEGIN

VAC_CLR_INT(0) <= NOT (VAC_CLR(0) OR FLT_CLR);
VAC_STATUS(0) <= VAC_IN0;

EN_VAC_FLT0 <= VAC_IN0 AND (NOT MASK_VAC(0));
VAC_FLT0_FLIP_FLOP:latch_n
        port map(clock => CLOCK,
             reset => RESET,
         clear => VAC_CLR_INT(0),
         en    => EN_VAC_FLT0,
         inp   => '1',
         oup   => VAC_FLT(0)
        );
VAC_CLR_INT(1) <= NOT (VAC_CLR(1) OR FLT_CLR);
VAC_STATUS(1) <= VAC_IN1;

EN_VAC_FLT1 <= VAC_IN1 AND (NOT MASK_VAC(1));
VAC_FLT1_FLIP_FLOP:latch_n
        port map(clock => CLOCK,
             reset => RESET,
         clear => VAC_CLR_INT(1),
         en    => EN_VAC_FLT1,
         inp   => '1',
         oup   => VAC_FLT(1)
        );

END ARCHITECTURE BEHAVIOR;

---------------------------end of vac fault status--------------------
---------------------------cavity fault generator---------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY CAV_FAULT_GEN IS
PORT(CLOCK : IN STD_LOGIC;
     RESET : IN STD_LOGIC;

     STP_TMP_FLT : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
     STP_TMP_MASK : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
     CPL_TMP_FLT : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     CPL_TMP_MASK : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
     VAC_FLT : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
     VAC_FLT_MASK : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
     HE_FLT : IN STD_LOGIC;
     HE_FLT_MASK : IN STD_LOGIC;
     CAV_FLT_OUT : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
     );
END ENTITY CAV_FAULT_GEN;
ARCHITECTURE BEHAVIOR OF CAV_FAULT_GEN IS
SIGNAL CAV_FLT_INT : STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN
CAV_FLT_INT_GEN_I: FOR I IN 0 TO 3 GENERATE
CAV_FLT_INT(I) <= (STP_TMP_FLT(I) AND (NOT STP_TMP_MASK(I))) OR
          (CPL_TMP_FLT(2*I) AND (NOT CPL_TMP_MASK(2*I))) OR
          (CPL_TMP_FLT((2*I)+1) AND (NOT CPL_TMP_MASK((2*I)+1))) OR
          (VAC_FLT(0) AND (NOT VAC_FLT_MASK(0))) OR
          (VAC_FLT(1) AND (NOT VAC_FLT_MASK(1))) OR
          (HE_FLT AND (NOT HE_FLT_MASK));

END GENERATE;

CAV_FLT_GEN_REG: REGNE
GENERIC MAP(N => 4)
PORT MAP(CLOCK      => CLOCK,
         RESET      => RESET,
         CLEAR      => '1',
         EN         => '1',
         INPUT      => CAV_FLT_INT,
     OUTPUT     => CAV_FLT_OUT
        );
END ARCHITECTURE BEHAVIOR;
--------------------end of cavity fault generator---------------------
--------------------cavity fault clear generator----------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY FLT_CLR_GEN IS
PORT(CLOCK : IN STD_LOGIC;
     RESET : IN STD_LOGIC;
     FLT_CLR_IN : IN STD_LOGIC;

     FLT_CLR_OUT : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
     );
END ENTITY FLT_CLR_GEN;

ARCHITECTURE BEHAVIOR OF FLT_CLR_GEN IS
SIGNAL FLT_CLR_INT0 : STD_LOGIC;
SIGNAL FLT_CLR_INT1 : STD_LOGIC;
SIGNAL FLT_CLR_INT2 : STD_LOGIC;
SIGNAL FLT_CLR_INT3 : STD_LOGIC;
SIGNAL FLT_CLR_INT4 : STD_LOGIC;
SIGNAL FLT_CLR_INT5 : STD_LOGIC;

BEGIN

FLT_CLR_FF0: FLIP_FLOP
        PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
             CLEAR  => '1',
             EN => '1',
             INP    => FLT_CLR_IN,
             OUP    => FLT_CLR_INT0
            );
FLT_CLR_FF1: FLIP_FLOP
        PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
         CLEAR  => '1',
         EN     => '1',
         INP    => FLT_CLR_INT0,
         OUP    => FLT_CLR_INT1
        );
FLT_CLR_FF2: FLIP_FLOP
        PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
         CLEAR  => '1',
         EN     => '1',
         INP    => FLT_CLR_INT1,
         OUP    => FLT_CLR_INT2
        );
FLT_CLR_FF3: FLIP_FLOP
        PORT MAP(CLOCK  => CLOCK,
         RESET  => RESET,
         CLEAR  => '1',
         EN     => '1',
         INP    => FLT_CLR_INT2,
         OUP    => FLT_CLR_INT3
        );
FLT_CLR_FF4: FLIP_FLOP
        PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
         CLEAR  => '1',
         EN     => '1',
         INP    => FLT_CLR_INT3,
         OUP    => FLT_CLR_INT4
        );

FLT_CLR_FF5: FLIP_FLOP
        PORT MAP(CLOCK  => CLOCK,
             RESET  => RESET,
         CLEAR  => '1',
         EN     => '1',
         INP    => FLT_CLR_INT4,
         OUP    => FLT_CLR_INT5
        );
FLT_CLR_OUT(0) <= FLT_CLR_INT1 AND (NOT FLT_CLR_INT2);
FLT_CLR_OUT(1) <= FLT_CLR_INT4 AND (NOT FLT_CLR_INT5);

END ARCHITECTURE BEHAVIOR;

-------------------------------end of fault clear generator---------------------
-------------------------------fsd fault generator------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.COMPONENTS.ALL;

ENTITY FC_FSD_OUT IS
PORT(CLOCK : IN STD_LOGIC;
     RESET : IN STD_LOGIC;

     CAV_FLT_IN : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
     FLT_CLR : IN STD_LOGIC;
     FC_FSD_MASK : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
     FC_FSD_CLR : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
     FC_FSD_STATUS : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
     );
END ENTITY FC_FSD_OUT;

ARCHITECTURE BEHAVIOR OF FC_FSD_OUT IS

SIGNAL FC_FSD_CLR_INT   : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL FC_FSD_FAULT : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL EN_FC_FSD    : STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN

FC_FSD_STATUS(3 DOWNTO 0) <= CAV_FLT_IN;
FC_FSD_STATUS(7 DOWNTO 4) <= FC_FSD_FAULT;

FC_FSD_LATCH_GEN_I: FOR I IN 0 TO 3 GENERATE
    FC_FSD_LATCHFFI1: LATCH_N
        PORT MAP(CLOCK => CLOCK,
             RESET => '1',
             CLEAR => FC_FSD_CLR_INT(I),
             EN => EN_FC_FSD(I),
             INP => '1',
             OUP => FC_FSD_FAULT(I)
             );
FC_FSD_CLR_INT(I) <= NOT (FC_FSD_CLR(I) OR FLT_CLR);
EN_FC_FSD(I) <= CAV_FLT_IN(I) AND (NOT FC_FSD_MASK(I));
END GENERATE;


END ARCHITECTURE BEHAVIOR;

-------------------------end of fsd fault generator-----------------------------
