library ieee;
use ieee.std_logic_1164.all;

entity led_test is
	port (
				 clock :in  std_logic;
				 reset :in  std_logic;

				 -- For driving the stepper FSM
				 enable    :in std_logic;
				 direction :in std_logic;
				 step_mode :in std_logic_vector(2 downto 0);

				 -- For controlling the LEDs
				 led_0_red   :out std_logic;
				 led_0_green :out std_logic;
				 led_0_blue  :out std_logic;
				 led_1_red   :out std_logic;
				 led_1_green :out std_logic;
				 led_1_blue  :out std_logic

);

end led_test;

architecture rtl of led_test is

begin

		-- Top FSM
	process(clock)
	begin
		if (rising_edge(clock)) then
			if (reset = '1' and enable = '0') then
					 -- Idle State -- In Reset
				led_0_red   <= '0';
				led_0_green <= '0';
				led_0_blue  <= '0';
				led_1_red   <= '0';
				led_1_green <= '0';
				led_1_blue  <= '0';

			elsif (enable = '0') then
				led_0_red   <= '1';
				led_0_green <= '0';
				led_0_blue  <= '0';
				led_1_red   <= '0';
				led_1_green <= '0';
				led_1_blue  <= '0';

			elsif (enable = '1') then
				-- Set direction LED
				if (direction = '1') then
					led_0_red   <= '0';
					led_0_green <= '1';
					led_0_blue  <= '0';
				elsif (direction = '0') then
					led_0_red   <= '0';
					led_0_green <= '0';
					led_0_blue  <= '1';
				else
					led_0_red   <= '1';
					led_0_green <= '0';
					led_0_blue  <= '0';
				end if;

				-- Set mode LED
				case step_mode is
					when "000" =>
						led_1_red   <= '0';
						led_1_green <= '0';
						led_1_blue  <= '0';

					when "001" =>
						led_1_red   <= '1';
						led_1_green <= '0';
						led_1_blue  <= '0';

					when "010" =>
						led_1_red   <= '0';
						led_1_green <= '1';
						led_1_blue  <= '0';

					when "011" =>
						led_1_red   <= '1';
						led_1_green <= '1';
						led_1_blue  <= '0';

					when "100" =>
						led_1_red   <= '0';
						led_1_green <= '0';
						led_1_blue  <= '1';

					when "101" =>
						led_1_red   <= '1';
						led_1_green <= '0';
						led_1_blue  <= '1';

					when "110" =>
						led_1_red   <= '0';
						led_1_green <= '1';
						led_1_blue  <= '1';

					when "111" =>
						led_1_red   <= '1';
						led_1_green <= '1';
						led_1_blue  <= '1';

					when others =>
						led_1_red   <= '0';
						led_1_green <= '0';
						led_1_blue  <= '0';

				end case;
			end if;
		end if;
	end process;

end architecture;
