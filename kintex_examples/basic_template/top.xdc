set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]

############################################
# Clocks
#

set_property IOSTANDARD LVDS_25 [get_ports clk_sys_*]
set_property DIFF_TERM TRUE [get_ports clk_sys_*]

set_property PACKAGE_PIN G11 [get_ports clk_sys_p]
set_property PACKAGE_PIN F10 [get_ports clk_sys_n]

create_clock -name clk_sys -period 20.0 [get_ports clk_sys_p]

############################################
# LEDs
#

set_property IOSTANDARD LVCMOS25 [get_ports leds[*]]
#set_property SLEW SLOW [get_ports ttl_lemo_outputs[*]]
#set_property DRIVE 4 [get_ports ttl_lemo_outputs[*]]

set_property PACKAGE_PIN M16 [get_ports leds[0]]
set_property PACKAGE_PIN K15 [get_ports leds[1]]

############################################
# Communications
#

set_property IOSTANDARD LVDS_25 [get_ports kintex_data_*]
set_property DIFF_TERM TRUE [get_ports kintex_data_in_*]

set_property PACKAGE_PIN J11 [get_ports kintex_data_out_p]
set_property PACKAGE_PIN J10 [get_ports kintex_data_out_n]

set_property PACKAGE_PIN J13 [get_ports kintex_data_in_p]
set_property PACKAGE_PIN H13 [get_ports kintex_data_in_n]

set_property DRIVE 4 [get_ports kintex_rx_locked]
set_property IOSTANDARD LVCMOS25 [get_ports kintex_rx_locked]
set_property PACKAGE_PIN F14 [get_ports kintex_rx_locked]

############################################
# User Controls
#

set_property IOSTANDARD LVCMOS18 [get_ports user_*]
set_property DRIVE 4 [get_ports user_*]

set_property PACKAGE_PIN V2  [get_ports user_switch]
set_property PACKAGE_PIN V1  [get_ports user_left_button]
set_property PACKAGE_PIN AF4 [get_ports user_right_button]
set_property PACKAGE_PIN Y5  [get_ports user_rotary[0]]
set_property PACKAGE_PIN AB6 [get_ports user_rotary[1]]
set_property PACKAGE_PIN AC6 [get_ports user_rotary[2]]
set_property PACKAGE_PIN AA4 [get_ports user_rotary[3]]
set_property PACKAGE_PIN AB4 [get_ports user_rotary[4]]
set_property PACKAGE_PIN AA3 [get_ports user_rotary[5]]
set_property PACKAGE_PIN AA2 [get_ports user_rotary[6]]
set_property PACKAGE_PIN AB1 [get_ports user_rotary[7]]

############################################
# LED Control
#

set_property IOSTANDARD LVCMOS33 [get_ports led_*]
set_property DRIVE 4 [get_ports led_*]

set_property PACKAGE_PIN L24 [get_ports led_sda]
set_property PACKAGE_PIN J26 [get_ports led_scl]

############################################
# Stepper Motor Control and Common
#

set_property IOSTANDARD LVCMOS33 [get_ports m_*]
set_property DRIVE 4 [get_ports m_*]

set_property PACKAGE_PIN P18 [get_ports m_sda]
set_property PACKAGE_PIN M21 [get_ports m_scl]

############################################
# Stepper Motor 1
#

set_property PACKAGE_PIN P24 [get_ports m_csn[0]]
set_property PACKAGE_PIN N24 [get_ports m_sclk[0]]
set_property PACKAGE_PIN T23 [get_ports m_sdo[0]]
set_property PACKAGE_PIN P19 [get_ports m_sdi[0]]
set_property PACKAGE_PIN P20 [get_ports m_en[0]]
set_property PACKAGE_PIN U17 [get_ports m_dir[0]]
set_property PACKAGE_PIN T17 [get_ports m_step[0]]
set_property PACKAGE_PIN M22 [get_ports m_lflf[0]]
set_property PACKAGE_PIN N23 [get_ports m_hflf[0]]

############################################
# Stepper Motor 2
#

set_property PACKAGE_PIN D26 [get_ports m_csn[1]]
set_property PACKAGE_PIN C26 [get_ports m_sclk[1]]
set_property PACKAGE_PIN B22 [get_ports m_sdo[1]]
set_property PACKAGE_PIN B24 [get_ports m_sdi[1]]
set_property PACKAGE_PIN A25 [get_ports m_en[1]]
set_property PACKAGE_PIN F22 [get_ports m_dir[1]]
set_property PACKAGE_PIN E23 [get_ports m_step[1]]
set_property PACKAGE_PIN A22 [get_ports m_lflf[1]]
set_property PACKAGE_PIN C24 [get_ports m_hflf[1]]

############################################
# Stepper Motor 3
#

set_property PACKAGE_PIN E25 [get_ports m_csn[2]]
set_property PACKAGE_PIN D25 [get_ports m_sclk[2]]
set_property PACKAGE_PIN G21 [get_ports m_sdo[2]]
set_property PACKAGE_PIN G25 [get_ports m_sdi[2]]
set_property PACKAGE_PIN G22 [get_ports m_en[2]]
set_property PACKAGE_PIN E21 [get_ports m_dir[2]]
set_property PACKAGE_PIN E22 [get_ports m_step[2]]
set_property PACKAGE_PIN A23 [get_ports m_lflf[2]]
set_property PACKAGE_PIN A24 [get_ports m_hflf[2]]

############################################
# Stepper Motor 4
#

set_property PACKAGE_PIN D21 [get_ports m_csn[3]]
set_property PACKAGE_PIN C22 [get_ports m_sclk[3]]
set_property PACKAGE_PIN C23 [get_ports m_sdo[3]]
set_property PACKAGE_PIN C21 [get_ports m_sdi[3]]
set_property PACKAGE_PIN B21 [get_ports m_en[3]]
set_property PACKAGE_PIN A20 [get_ports m_dir[3]]
set_property PACKAGE_PIN B20 [get_ports m_step[3]]
set_property PACKAGE_PIN G26 [get_ports m_lflf[3]]
set_property PACKAGE_PIN H21 [get_ports m_hflf[3]]

